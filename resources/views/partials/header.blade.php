<?php global $swu_phone, $swu_phone_stripped, $swu_desktop_logo; ?>

<header class="banner uk-text-center">
  <div class=" gridxl">
    <div class="uk-grid uk-grid-collapse">
      <div class="uk-width-1-5@m brand">
        <a href="/prime"><span class="headerbrand">PRIME</span></a>
      </div>
      <div class="uk-width-2-5@m nav">
        <ul>
          <li><a href="/prime/start-here/" class="bold color-black">Start Here</a></li>
          <li><a href="/prime/about-prime/" class="bold color-black">About</a></li>
          <li><a href="/prime/for-him/" class="bold color-black">For Him</a></li>
          <li><a href="/prime/videos/" class="bold color-black">Videos</a></li>
          {{-- <li><a href="/directory-men/" class="bold color-black">For Her</a></li> --}}
          <li><a href="/directory-men/" class="bold color-black">Directory</a></li>
        </ul>
      </div>
      <div class="uk-width-2-5@m uk-text-right rightmenu">
        <ul>
          <li><a href="/prime/for-providers/" class="bold accent">For Providers</a></li>
          {{-- <li class="bold color-black">Login</li> --}}
        </ul>
      </div>
    </div>
  </div>
  <div class="offcanv">
    <a href="#my-id" uk-toggle>
      <span uk-icon="icon: menu"></span>
    </a>
  </div>
</header>
<div class="spacer" style="height: 89px;"></div>

<div id="my-id" uk-offcanvas="overlay: true">
  <div class="uk-offcanvas-bar">
    <div class="uk-text-center head">
      <span class="headerbrand">PRIME</span>
    </div>
    <ul class="menu">
      <li><a href="/prime/start-here/" class="bold color-black">Start Here</a></li>
      <li><a href="/prime/about-prime/" class="bold color-black">About</a></li>
      <li><a href="/prime/for-him/" class="bold color-black">For Him</a></li>
      {{-- <li><a href="/directory-men/" class="bold color-black">For Her</a></li> --}}
      <li><a href="/directory-men/" class="bold color-black">Directory</a></li>
      <li><a href="/prime/for-providers/" class="bold accent">For Providers</a></li>
      {{-- <li class="bold color-black">Login</li> --}}
    </ul>
  </div>
</div>
