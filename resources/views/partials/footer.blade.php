<?php global $swu_phone, $swu_phone_stripped, $swu_desktop_logo, $swu_email; ?>
<footer class="banner uk-block bg6">
  <div class="gridl">
    <div class="uk-grid uk-grid-collapse uk-text-center uk-text-left@s">
      <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-5@m">
        <h2 class="color-white">PRIME</h2>
      </div>
      <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-5@m">
        <h4 class="color-white">About us</h4>
        <ul>
          <li>Company Overview</li>
          <li>Our Strategy</li>
          <li>Our History</li>
          <li>Board of directors</li>
          <li>FAQs</li>
        </ul>
      </div>
      <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-5@m">
        <h4 class="color-white">Company</h4>
        <ul>
          <li>Company Overview</li>
          <li>Our Strategy</li>
          <li>Our History</li>
          <li>Board of directors</li>
          <li>FAQs</li>
        </ul>
      </div>
      <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-5@m">
        <h4 class="color-white">Links</h4>
        <ul>
          <li>Company Overview</li>
          <li>Our Strategy</li>
          <li>Our History</li>
          <li>Board of directors</li>
          <li>FAQs</li>
          <li>FAQs</li>
        </ul>
      </div>
      <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-5@m" id="questions">
        <h4 class="color-white">Have a Question?</h4>
        <a href="#" class="help">Help</a>
        <a href="#" class="btn btn-green2 squared">Hello@prime.com</a>
      </div>
    </div>
  </div>
</footer>