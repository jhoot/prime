{{--
  Template Name: Stacks Template
--}}

@extends('layouts.app')

@section('content')
<style>
#stackfold {
  min-height: 80vh;
  padding-bottom: 200px;
}
#stackfold .h1 {
  padding-top: 100px;
}

.stackcontent div.nav {
  background: #3C4F65;
}
.stackcontent div.nav ul.uk-grid {
  text-align: center;
}
.stackcontent div.nav ul.uk-grid li {
  padding: 40px;
  transition: 0.25s all ease-in-out;
}
.stackcontent div.nav ul.uk-grid li a {
  color: white;
  text-decoration: none;
  letter-spacing: 0.05em;
}
.stackcontent div.nav ul.uk-grid li.uk-active {
  background: #FF5733;
}
.stackcontent div.nav ul.uk-grid li.uk-active a {
  font-weight: bold;
}
.stackcontent div.nav ul.uk-grid li:hover a {
  text-decoration: none;
}

.bgcream {
  background: #FFF5EF;
}

.color-orange {
  color: #FF8162;
}

.color-teal {
  color: #41AAA8;
}

.bgteal {
  background: #E8F6F6;
}

.bgorange {
  background: #FBECE2;
}

.steps .gridxs .uk-padding .uk-grid .uk-width-1-6 {
  margin-top: 5px;
}
.steps .gridxs .uk-padding .uk-grid .uk-width-1-6 .stepcircle {
  background: #007AFF;
  margin-top: 10px;
  padding: 13px 15px;
  text-align: center;
  border-radius: 35px;
}
.steps .gridxs .uk-padding .uk-grid .uk-width-1-6 .stepcircle span {
  font-size: 125%;
}
.steps .gridxs .uk-padding .uk-grid .uk-width-5-6 {
  margin-bottom: 15px;
}

.whatifelt .gridl .uk-grid .uk-width-1-1 img {
  max-width: 80%;
}

.current .gridxl, .adding .gridxl {
  position: relative;
}
.current .gridxl .lefttext, .adding .gridxl .lefttext {
  position: absolute;
  left: -60px;
  top: 20px;
}
.current .gridxl .lefttext h3, .adding .gridxl .lefttext h3 {
  text-transform: uppercase;
  letter-spacing: 1.5px;
}
.current .gridxl .uk-grid .uk-width-1-1 .bgorange h4, .current .gridxl .uk-grid .uk-width-1-1 .bgteal h4, .adding .gridxl .uk-grid .uk-width-1-1 .bgorange h4, .adding .gridxl .uk-grid .uk-width-1-1 .bgteal h4 {
  font-size: 160% !important;
}
.current .gridxl .uk-grid .uk-width-1-1 .bgorange ul, .current .gridxl .uk-grid .uk-width-1-1 .bgteal ul, .adding .gridxl .uk-grid .uk-width-1-1 .bgorange ul, .adding .gridxl .uk-grid .uk-width-1-1 .bgteal ul {
  list-style-type: none;
  padding-left: 0;
}
.current .gridxl .uk-grid .uk-width-1-1 .bgorange ul li, .current .gridxl .uk-grid .uk-width-1-1 .bgteal ul li, .adding .gridxl .uk-grid .uk-width-1-1 .bgorange ul li, .adding .gridxl .uk-grid .uk-width-1-1 .bgteal ul li {
  padding: 10px 5px;
}

.adding {
  padding-bottom: 200px;
}

.limec {
  color: #6EFFBF;
}

.keypoints {
  margin-top: -75px;
}
.keypoints .uk-grid .uk-width-1-1 .bg3 {
  width: 150px;
  height: 150px;
  margin: 0 auto;
  text-align: center;
  border-radius: 150px;
}
.keypoints .uk-grid .uk-width-1-1 .bg3 h4 {
  padding-top: 50px;
}

.testresults {
  margin-top: -75px;
}
.testresults .gridm .uk-block h2 {
  margin-bottom: 20px;
}
.testresults .gridm .uk-block a.btn.squared.lime {
  background: #6EFFBF;
  padding: 10px 25px;
  color: black;
}

@media (max-width: 1500px) {
  .current .gridxl .lefttext, .adding .gridxl .lefttext {
    left: -40px;
  }
  .current .gridxl .uk-grid, .adding .gridxl .uk-grid {
    margin-right: -40px;
  }
}
@media (max-width: 1400px) {
  .current .gridxl .lefttext, .adding .gridxl .lefttext {
    top: -40px;
    left: 0;
    width: 100%;
    text-align: center;
  }
  .current .gridxl .lefttext h3, .adding .gridxl .lefttext h3 {
    transform: none !important;
    writing-mode: initial;
  }
  .current .gridxl .uk-grid, .adding .gridxl .uk-grid {
    margin-right: 0;
  }
  .current .gridxl .uk-grid .uk-width-1-1, .adding .gridxl .uk-grid .uk-width-1-1 {
    margin: 10px 0;
  }
}
@media (max-width: 959px) {
  .uk-grid div {
    margin: 10px auto;
  }
}
@media (max-width: 639px) {
  .uk-grid {
    margin-left: 0;
  }
  .uk-grid div {
    padding-left: 0;
    margin: 10px auto;
  }

  .uk-grid-medium {
    margin-left: 0;
  }
}
</style>










  <section id="stackfold" class="uk-block uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/stacksbg.png');">
    <div class="gridxl h1 uk-text-center">
      <h1 class="color-black bold underlined"><?php the_field('header'); ?></h1>
    </div>
    <div class="gridxl img">
      <img src="/wp-content/uploads/2019/01/stacks.png">
    </div>
    <div class="grids p uk-text-center">
      <p class="color-black" style="font-size: 125%;"><?php the_field('copy'); ?></p>
    </div>
  </section>
  
  <?php $navargs = array(
      'post_type' => 'stacks',
      'posts_per_page' => 6,
      'order' => 'ASC'
  ); 
  
  $navquery = new WP_Query($navargs);

  ?>
  
  <section class="stackcontent">
    <div class="nav">
      <?php if($navquery->have_posts()) : ?>
        <ul class="uk-child-width-1-1 uk-text-center uk-container-center uk-child-width-1-3@s uk-child-width-1-6@m uk-grid" uk-switcher="connect: .switchercontent">
          <?php while($navquery->have_posts()): $navquery->the_post(); ?>
          <li>
            <a href="#"><?php the_title(); ?></a>
          </li>
        <?php endwhile; wp_reset_postdata(); ?>
        </ul>
      <?php endif; ?>
    </div>
    <div class="bg5">
        <?php $contentquery = new WP_Query($navargs); 
        if($contentquery->have_posts()):
        ?>
        <ul class="uk-switcher switchercontent">
          <?php while($contentquery->have_posts()): $contentquery->the_post(); ?>
          <li>
            <div class="gridm bg-white">
              <div class="uk-padding-large">
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-2-3@s">
                    <div class="uk-grid uk-grid-large uk-text-center uk-container-center">
                      <div class="uk-width-1-1 uk-width-1-2@s profimg uk-text-center uk-text-right@s">
                        <img src="<?php the_field('image'); ?>" style="max-width: 200px;">
                      </div>
                      <div class="uk-width-1-1 uk-width-1-2@s profbio uk-text-center uk-text-left@s">
                        <div class="uk-block-small">
                          <span class="color-black"><span class="bold">Name:</span> <?php the_field('name'); ?></span><br><br>
                          <span class="color-black"><span class="bold">Height:</span> <?php the_field('height'); ?></span><br><br>
                          <span class="color-black"><span class="bold">Weight:</span> <?php the_field('weight'); ?></span><br><br>
                          <span class="color-black"><span class="bold">Age:</span> <?php the_field('age'); ?></span><br><br>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-text-center uk-text-left@s">
                    <div class="uk-block-small">
                      <span class="color-black"><span class="bold">Origin:</span> <?php the_field('origin'); ?></span><br><br>
                      <span class="color-black"><span class="bold">Occupation:</span> <?php the_field('occupation'); ?></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="gridm uk-text-center">
              <div class="uk-block-large">
                <h2 class="color-black bold">Health Moment:</h2>
                <h3 class="color-black"><?php the_field('health_moment'); ?></h3>
              </div>
            </div>
            <div class="gridxl uk-text-center">
              <img src="<?php the_field('central_picture'); ?>">
            </div>
            <div class="uk-block steps bgcream">
              <div class="gridxs bg-white">
                <div class="uk-padding">
                  <?php if(have_rows('steps')): ?>
                  <div class="uk-grid uk-grid-small">
                    <?php $stepscount = 1; ?>
                    <?php while(have_rows('steps')): the_row(); ?>
                    <div class="uk-width-1-6">
                      <div class="bg3 stepcircle">
                        <span class="color-white"><?= $stepscount; ?></span>
                      </div>
                    </div>
                    <div class="uk-width-5-6">
                      <p class="color-black"><?php the_sub_field('step'); ?></p>
                    </div>
                    <?php $stepscount++; ?>
                  <?php endwhile; ?>
                  </div>
                <?php endif; ?>
                </div>
              </div>
            </div>
            <div class="whatifelt uk-block bgcream">
              <div class="gridl">
                <div class="uk-padding">
                  <h2 class="color-black bold" style="text-transform: uppercase;">What I Felt</h2>
                </div>
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-1-2@m uk-text-center">
                    <img src="<?php the_field('felt_image'); ?>">
                  </div>
                  <div class="uk-width-1-1 uk-width-1-2@m">
                    <?= get_field('felt'); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="whatiwant uk-block bgcream">
              <div class="gridl">
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-1-2@m uk-text-center">
                    <img src="<?php the_field('want_image'); ?>" style="max-height: 550px;">
                  </div>
                  <div class="uk-width-1-1 uk-width-1-2@m">
                    <div class="uk-padding">
                        <?= get_field('want_copy'); ?>
                    </div>
                    <div class="uk-block">
                      <h2 class="color3 bold" style="text-transform: uppercase;"><?php the_field('end_quote'); ?></h2>
                    </div>
                  </div>
                </div>
                <div class="uk-block">
                  <div class="uk-grid">
                    <div class="uk-width-1-1 uk-width-2-3@m">
                      <div class="whatiwant">
                        <h3 class="color-black bold"><?php the_field('want_header'); ?></h3>
                        <p class="color-black"><?php the_field('want_sub'); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="bg-white uk-block current">
              <div class="gridxl">
                <div class="lefttext">
                  <h3 class="color-orange bold rotated">Current</h3>
                </div>
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                    <div class="bgorange uk-padding-small">
                      <h4 class="color-orange bold">Exercise</h4>
                      <?php if(have_rows('current_exercise')): ?>
                        <ul>
                          <?php while(have_rows('current_exercise')): the_row(); ?>
                            <li class="color-black"><?php the_sub_field('item'); ?></li>
                          <?php endwhile; ?>
                        </ul>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgorange uk-padding-small">
                        <h4 class="color-orange bold">Supplements</h4>
                        <?php if(have_rows('current_supplements')): ?>
                          <ul>
                            <?php while(have_rows('current_supplements')): the_row(); ?>
                              <li class="color-black"><?php the_sub_field('item'); ?></li>
                            <?php endwhile; ?>
                          </ul>
                        <?php endif; ?>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgorange uk-padding-small">
                        <h4 class="color-orange bold">Mindset</h4>
                        <?php if(have_rows('current_mindset')): ?>
                          <ul>
                            <?php while(have_rows('current_mindset')): the_row(); ?>
                              <li class="color-black"><?php the_sub_field('item'); ?></li>
                            <?php endwhile; ?>
                          </ul>
                        <?php endif; ?>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgorange uk-padding-small">
                        <h4 class="color-orange bold">Products</h4>
                        <?php if(have_rows('current_products')): ?>
                          <ul>
                            <?php while(have_rows('current_products')): the_row(); ?>
                              <li class="color-black"><?php the_sub_field('item'); ?></li>
                            <?php endwhile; ?>
                          </ul>
                        <?php endif; ?>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgorange uk-padding-small">
                        <h4 class="color-orange bold">Test</h4>
                        <?php if(have_rows('current_test')): ?>
                          <ul>
                            <?php while(have_rows('current_test')): the_row(); ?>
                              <li class="color-black"><?php the_sub_field('item'); ?></li>
                            <?php endwhile; ?>
                          </ul>
                        <?php endif; ?>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                    <div class="bgorange uk-padding-small">
                      <h4 class="color-orange bold">Pharma</h4>
                      <?php if(have_rows('current_pharma')): ?>
                        <ul>
                          <?php while(have_rows('current_pharma')): the_row(); ?>
                            <li class="color-black"><?php the_sub_field('item'); ?></li>
                          <?php endwhile; ?>
                        </ul>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="bg-white adding">
              <div class="gridxl">
                <div class="lefttext">
                  <h3 class="color-teal bold rotated">Adding</h3>
                </div>
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                    <div class="bgteal uk-padding-small">
                      <h4 class="color-teal bold">Exercise</h4>
                      <?php if(have_rows('adding_exercise')): ?>
                        <ul>
                          <?php while(have_rows('adding_exercise')): the_row(); ?>
                            <li class="color-black"><?php the_sub_field('item'); ?></li>
                          <?php endwhile; ?>
                        </ul>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgteal uk-padding-small">
                        <h4 class="color-teal bold">Supplements</h4>
                        <?php if(have_rows('adding_supplements')): ?>
                          <ul>
                            <?php while(have_rows('adding_supplements')): the_row(); ?>
                              <li class="color-black"><?php the_sub_field('item'); ?></li>
                            <?php endwhile; ?>
                          </ul>
                        <?php endif; ?>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgteal uk-padding-small">
                        <h4 class="color-teal bold">Mindset</h4>
                        <?php if(have_rows('adding_mindset')): ?>
                          <ul>
                            <?php while(have_rows('adding_mindset')): the_row(); ?>
                              <li class="color-black"><?php the_sub_field('item'); ?></li>
                            <?php endwhile; ?>
                          </ul>
                        <?php endif; ?>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgteal uk-padding-small">
                        <h4 class="color-teal bold">Products</h4>
                        <?php if(have_rows('adding_products')): ?>
                          <ul>
                            <?php while(have_rows('adding_products')): the_row(); ?>
                              <li class="color-black"><?php the_sub_field('item'); ?></li>
                            <?php endwhile; ?>
                          </ul>
                        <?php endif; ?>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgteal uk-padding-small">
                        <h4 class="color-teal bold">Test</h4>
                        <?php if(have_rows('adding_test')): ?>
                          <ul>
                            <?php while(have_rows('adding_test')): the_row(); ?>
                              <li class="color-black"><?php the_sub_field('item'); ?></li>
                            <?php endwhile; ?>
                          </ul>
                        <?php endif; ?>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                    <div class="bgteal uk-padding-small">
                      <h4 class="color-teal bold">Pharma</h4>
                      <?php if(have_rows('adding_pharma')): ?>
                        <ul>
                          <?php while(have_rows('adding_pharma')): the_row(); ?>
                            <li class="color-black"><?php the_sub_field('item'); ?></li>
                          <?php endwhile; ?>
                        </ul>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="keypoints">
              <?php if(have_rows('supps')): ?>
                <div class="uk-grid uk-grid-large uk-text-center uk-container-center">
                  <?php $suppscount = count(get_field('supps')); ?>

                  <?php while(have_rows('supps')): the_row(); ?>
                    <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-<?= $suppscount; ?>@m uk-text-center">
                      <div class="bg3 circ">
                        <h4 class="color-white bold"><?php the_sub_field('item'); ?></h4>
                      </div>
                    </div>
                  <?php endwhile; ?>
                </div>
              <?php endif; ?>
            <div class="testresults uk-block-large uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/testresultsbg.png');">
              <div class="gridm">
                <div class="uk-block uk-text-center">
                  <h2 class="limec bold">Download all of my test results</h2>
                  <div class="spacer" style="height: 30px;">

                  </div>
                  <a href="<?php the_field('test_results'); ?>" class="btn squared lime">Download Now</a>
                </div>
              </div>
            </div>
          </li>
        <?php endwhile; wp_reset_postdata(); ?>
        </ul>
      <?php endif; ?>
    </div>
  </section>
@endsection