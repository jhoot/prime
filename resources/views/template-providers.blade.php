{{--
  Template Name: Provider Template
--}}

@extends('layouts.app')

@section('content')
<section class="uk-block-large providers-fold bg5" style="background-image: url('/prime/wp-content/uploads/2019/01/providersbg2.png'); background-size: cover; background-position: 50%;">
  <div class="grids uk-text-center">
    <h2 class="color-white bold"><?php the_field('page_header'); ?></h2>
    <p class="color-white bold"><?php the_field('page_sub_header'); ?></p>
  </div>
  <div class="img">
    <img src="/prime/wp-content/uploads/2019/01/providers.png">
  </div>
</section>

<section class="providers-sub-fold uk-block bg5">
  <div class="gridxl">
    <div class="uk-grid uk-grid-collapse  uk-text-center uk-text-left@m">
      <div class="uk-width-1-1 uk-width-3-4@m main uk-text-center">
        <img src="/prime/wp-content/uploads/2019/01/circleproviders.png" alt="">
        <div class="content rounded bg-white uk-text-left uk-padding-small">
          <h4 class="color-black"><?php the_field('header_one'); ?></h4>
          <span><?php the_field('copy_one'); ?></span>
        </div>
      </div>
      <div class="uk-width-1-1 uk-width-1-4@m sidebar claim-form">
        <div class="header uk-text-center uk-background-cover uk-padding-small" style="background-image: url('/prime/wp-content/uploads/2019/01/claimbg.png');">
          <h4 class="color-white bold">Claim your profile</h4>
          
        </div>
        <div class="form uk-background-cover uk-padding-small" style="background-image: url('/prime/wp-content/uploads/2019/01/formbluewave.png');">
          <?= do_shortcode('[contact-form-7 id="52" title="claimprofile"]'); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="uk-block bg5">
  <div class="gridl uk-text-center">
    <h2 class="color-black"><?php the_field('header_two'); ?></h2>
  </div>
</section>

<section class="uk-block bg5">
  <div class="gridxl">
    <div class="uk-grid uk-grid-collapse">
      <div class="uk-width-1-1 uk-width-1-3@m uk-padding">
        <h2 class="color-black bold"><?php the_field('header_three'); ?></h2>
        <?= get_field('copy_three'); ?>
      </div>
      <div class="uk-width-1-1 uk-width-2-3@m uk-text-center">
        <img src="/prime/wp-content/uploads/2019/01/primelight.png" alt="">
      </div>
    </div>
  </div>
</section>

<section id="primepro" class="uk-block bg5">
  <div class="gridxl uk-background-cover" style="background-image: url('/prime/wp-content/uploads/2019/01/primeprobg.png');">
    <div class="uk-padding">
      <div class="uk-block-small">
        <h2 class="color-white"><?php the_field('pro_header'); ?></h2>
      </div>
      <div class="uk-grid uk-grid-large uk-container-center uk-text-center uk-text-left@s">
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <img src="/prime/wp-content/uploads/2019/01/iconprofile.png">
          <p>Enhanced Profile w/ Photos, Awards, Mission Statements</p>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <img src="/prime/wp-content/uploads/2019/01/icontraffic.png">
          <p>We Get You Targeted Local Traffic</p>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <img src="/prime/wp-content/uploads/2019/01/iconvideos.png">
          <p>Become a Thought Leader with Articles & Videos that live forever</p>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <img src="/prime/wp-content/uploads/2019/01/iconmegaphone.png">
          <p>Block Competitor Ads while Getting Promoted on Nearby Providers</p>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <img src="/prime/wp-content/uploads/2019/01/iconcare.png">
          <p>Attract Motivated Customers Looking for a Change Now</p>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <img src="/prime/wp-content/uploads/2019/01/iconattract.png">
          <p>Prime Custom Guidance and Advice on Business Growth</p>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@m buttonblock uk-text-center">
          <a href="#" class="btn">Claim My Profile</a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="uk-block uk-background-cover" style="background-image: url('/prime/wp-content/uploads/2019/01/talkthrough.png');">
  <div class="gridm">
    <div class="uk-block uk-text-center">
      <h2 class="color-white bold">Wanna Talk It Through?</h2>
    </div>
    <div class="container bg5 uk-padding">
      <div class="inner-container">
        <?= do_shortcode('[contact-form-7 id="63" title="talkthrough"]'); ?>
      </div>
    </div>
  </div>
</section>

@endsection