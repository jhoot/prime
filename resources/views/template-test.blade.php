{{--
  Template Name: TEST Template
--}}

@extends('layouts.app')

@section('content')


  <?php 

    global $wpdb;
    $results = $wpdb->get_results( "SELECT * FROM pv_drts_entity_field_entity_featured");
    $listingids = [];
    foreach ($results as $result) {
      array_push($listingids, $result->entity_id);
    }
  ?>
<?php
  $args = array(
    'post_type' => 'men_dir_ltg',
    'posts_per_page' => -1,
    'post__in'      => $listingids
  );
  $query = new WP_Query($args);
  if($query->have_posts()):
    while($query->have_posts()): $query->the_post();
      $content = get_the_content();
      print_r($content);
    endwhile;
    wp_reset_postdata();
  endif;





  ?>

@endsection