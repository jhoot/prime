{{--
  Template Name: Start Here Template
--}}

@extends('layouts.app')

@section('content')
  <section id="startfold" class="uk-block bg3">
    <div class="gridxl">
      <div class="uk-grid uk-grid-medium">
        <div class="left uk-width-1-1 uk-width-1-2@m">
          <img src="/prime/wp-content/uploads/2019/01/playbtn.png" alt="">
          <div class="content uk-padding-large">
            <h1 class="color-white bold"><?php the_field('header_one'); ?></h1>
            <p class="color-white"><?php the_field('copy_one'); ?></p>
          </div>
        </div>
        <div class="rightimg uk-width-1-1 uk-width-1-2@m">
          <img src="<?php the_field('image_one'); ?>" alt="">
        </div>
        <div class="leftimg uk-width-1-1 uk-width-1-2@m">
          <img src="<?php the_field('image_two'); ?>" alt="">
        </div>
        <div class="right uk-width-1-1 uk-width-1-2@m">
          <div class="content uk-padding-large">
            <?= get_field('copy_two'); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="ctapagebrk" class="uk-block bgdark">
    <div class="gridl">
      <div class="uk-grid uk-grid-large uk-text-center uk-text-left@m">
        <div class="uk-width-1-1 uk-width-1-2@m content">
          <p class="color-white"><?php the_field('page_break_copy'); ?></p>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@m form">
          <!-- Begin Mailchimp Signup Form -->
          <div id="mc_embed_signup3">
              <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                  <div id="mc_embed_signup_scroll">

                      <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                      </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                  </div>
              </form>
            </div>
        </div>
      </div>
    </div>
    <span class="left">PRIME</span>
    <span class="right">PRIME</span>
  </section>

  <section id="belowbrk" class="uk-block uk-background-cover" style="background-image: url('/prime/wp-content/uploads/2019/01/bgbelowbrkstart.png');">
    <div class="gridl">
      <div class="uk-flex uk-flex-right">
        <div class="content">
          <img src="/prime/wp-content/uploads/2019/01/primeblurbcirc.png" alt="">
          <div class="uk-text-left text">
            <p class="color-black"><?php the_field('prime_copy_one'); ?></p>
          </div>
        </div>
      </div>
      <div class="bottomtxt uk-block-small bg-white">
        <p class="color-black"><?php the_field('prime_copy_two'); ?></p>
      </div>
    </div>
  </section>

  <section class="bgpeach aboutlink uk-block">
    <div class="gridl">
      <div class="image uk-text-center">
        <img src="<?php the_field('about_inage'); ?>" alt="">
      </div>
      <div class="copy uk-text-center uk-padding-small">
        <h2 class="color-black bold"><?php the_field('about_header'); ?></h2>
      </div>
    </div>
    <div class="bg3 uk-padding-small">
      <a href="/about-prime/">
        <span class="color-white rotated">Learn more<br>about us</span>
      </a>      
    </div>
  </section>

  <section id="explainer" class="uk-block bg5">
    <div class="gridl">
      <div class="uk-grid uk-grid-large">
        <div class="uk-width-1-1 uk-width-1-2@m">
          <div class="text uk-padding-large">
            <?= get_field('copy_left'); ?>
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@m">
          <img src="<?php the_field('image_right'); ?>" alt="">
        </div>
        <div class="uk-width-1-1 uk-width-1-2@m">
          <img src="<?php the_field('image_left'); ?>" alt="">
        </div>
        <div class="bottomcopy uk-width-1-1 uk-width-1-2@m">
          <div class="text">
            <h3 class="color-black bold"><?php the_field('header_right'); ?></h3>
            <br>
            <div class="copy">
              <p class="color-black"><?php the_field('copy_right'); ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="uk-padding-large bg5 uk-text-center">
    <img src="/prime/wp-content/uploads/2019/01/result.png" alt="">
  </section>

  <section class="bg-white uk-block-small">
    <div class="gridl">
      <div class="uk-grid uk-grid-large uk-text-center uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-4@m">
        <a href="#guides" uk-scroll><h4 class="color-black">Guides</h4></a>
        <a href="#providers" uk-scroll><h4 class="color-black">Providers</h4></a>
        <a href="#reviews" uk-scroll><h4 class="color-black">Reviews</h4></a>
        <a href="#mystacks" uk-scroll><h4 class="color-black">My Stacks</h4></a>
      </div>
    </div>
  </section>
  <?php $gargs = array(
    'post_type' => 'guides',
    'posts_per_page' => 9,
    'order' => 'ASC'
  );
  
  $gquery = new WP_Query($gargs);

  if($gquery->have_posts()):
  ?>
  <section id="guides" class="uk-block bg5">
    <div class="gridxl">
      <div uk-slider="finite: true">
        <div class="uk-slider-container uk-text-center">
          <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-4@m">
            <li class="copy uk-text-left uk-padding">
              <h2 class="color-black bold">Guides</h2>
              <p class="color-black">Next, start thinking about what you really want to optimize, and what you're willing to give, both from time and money.</p>
              <br>
              <p class="color-black">Then either get yourself tested to learn more about your unique self using one of these kits.</p>
            </li>
            <?php while($gquery->have_posts()): $gquery->the_post(); ?>
            <li class="guide">
              <div class="container">
                <a href="<?php the_permalink(); ?>">
                  <img src="<?php the_field('image'); ?>" alt="">
                  <div class="text">
                    <span class="thetitle color-black bold"><?php the_field('title'); ?></span>
                    <span class="thecategory color-white"><?php the_field('category'); ?></span>
                  </div>
                </a>
              </div>
            </li>
            <?php endwhile; wp_reset_postdata(); ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="bg3 signupstart uk-padding-small">
      <!-- Begin Mailchimp Signup Form -->
      <div id="mc_embed_signupstart">
          <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
              <div id="mc_embed_signup_scroll">

                  <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                  </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                    <textarea type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address"></textarea>
                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
              </div>
          </form>
        </div>
    </div>
  </section>
<?php endif; ?>
<?php $pargs = array(
    'post_type' => 'providers',
    'posts_per_page' => 9,
    'order' => 'ASC'
  );
  
  $pquery = new WP_Query($pargs);

  if($pquery->have_posts()):
  ?>
  <section id="providers" class="uk-block bg-white">
    <div class="gridxl">
      <div uk-slider="finite: true">
        <div class="uk-slider-container uk-text-center">
          <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-4@m">
            <li class="copy uk-text-left uk-padding">
              <h2 class="color-black bold">Providers</h2>
              <p class="color-black">Next, start thinking about what you really want to optimize, and what you're willing to give, both from time and money.</p>
              <br>
              <p class="color-black">Then either get yourself tested to learn more about your unique self using one of these kits.</p>
            </li>
            <?php while($pquery->have_posts()): $pquery->the_post(); ?>
              <li class="guide">
                <div class="single-p uk-width-1-1">
                  <div class="bg">
                    <a href="<?php the_permalink(); ?>">
                      <div class="container">
                        <img src="<?php the_field('image'); ?>" alt="">
                        <div class="text uk-text-left">
                          <h4 class="name"><?php the_field('name'); ?></h4>
                          <span class="color-black"><?php the_field('title'); ?></span>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </li>
            <?php endwhile; wp_reset_postdata(); ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="bg3 signupstart uk-padding-small">
      <!-- Begin Mailchimp Signup Form -->
      <div id="mc_embed_signupstart">
          <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
              <div id="mc_embed_signup_scroll">

                  <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                  </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                    <textarea type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address"></textarea>
                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
              </div>
          </form>
        </div>
    </div>
  </section>

<?php endif; ?>

<?php $rargs = array(
    'post_type' => 'providers',
    'posts_per_page' => 9,
    'order' => 'ASC'
  );
  
  $rquery = new WP_Query($rargs);

  if($rquery->have_posts()):
  ?>
  <section id="reviews" class="uk-block bg5">
    <div class="gridxl">
      <div uk-slider="finite: true">
        <div class="uk-slider-container uk-text-center">
          <ul class="uk-slider-items uk-child-width-1-1">
            <li class="copy uk-text-left uk-padding uk-width-1-4">
              <h2 class="color-black bold">Reviews</h2>
              <p class="color-black">Next, start thinking about what you really want to optimize, and what you're willing to give, both from time and money.</p>
              <br>
              <p class="color-black">Then either get yourself tested to learn more about your unique self using one of these kits.</p>
            </li>
            <?php while($rquery->have_posts()): $rquery->the_post(); ?>
              <li class="guide">
                <div class="container">
                  <div class="single-product uk-block">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="uk-width-1-3 border">
                        <img src="<?php the_field('image'); ?>" alt="">
                      </div>
                      <div class="uk-width-2-3 text">
                        <div class="container">
                          <h4 class="color-black"><?php the_field('title'); ?></h4>
                          <p class="color-black"><?php the_field('description'); ?></p>
                          <span class="color-black"><span class="bold"><?php the_field('num_positive'); ?></span> Positive Reviews</span>
                          <a href="<?php the_permalink(); ?>" class="btn btn-black squared  ">Learn More</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            <?php endwhile; wp_reset_postdata(); ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="bg3 signupstart uk-padding-small">
      <!-- Begin Mailchimp Signup Form -->
      <div id="mc_embed_signupstart">
          <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
              <div id="mc_embed_signup_scroll">

                  <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                  </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                    <textarea type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address"></textarea>
                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
              </div>
          </form>
        </div>
    </div>
  </section>

<?php endif; ?>


<?php $sargs = array(
    'post_type' => 'providers',
    'posts_per_page' => 9,
    'order' => 'ASC'
  );
  
  $squery = new WP_Query($sargs);

  if($squery->have_posts()):
  ?>
  <section id="mystacks" class="uk-block bg-white">
    <div class="gridxl">
      <div uk-slider="finite: true">
        <div class="uk-slider-container uk-text-center">
          <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-4@m">
            <li class="copy uk-text-left uk-padding">
              <h2 class="color-black bold">My Stacks</h2>
              <p class="color-black">Next, start thinking about what you really want to optimize, and what you're willing to give, both from time and money.</p>
              <br>
              <p class="color-black">Then either get yourself tested to learn more about your unique self using one of these kits.</p>
            </li>
            <?php while($squery->have_posts()): $squery->the_post(); ?>
              <li class="guide">
                <div class="container">
                  <img src="<?php the_field('image'); ?>" alt="">
                  <div class="text uk-text-left">
                    <span class="thetitle color-white"><?php the_field('description'); ?></span>
                  </div>
                </div>
              </li>
            <?php endwhile; wp_reset_postdata(); ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="bg3 signupstart uk-padding-small">
      <!-- Begin Mailchimp Signup Form -->
      <div id="mc_embed_signupstart">
          <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
              <div id="mc_embed_signup_scroll">

                  <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                  </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                    <textarea type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address"></textarea>
                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
              </div>
          </form>
        </div>
    </div>
  </section>

<?php endif; ?>








  <section class="uk-block-large bg3">
    <div class="gridl uk-text-center">
      <h2 class="color2 bold">Bringing all of the different parts of ecosystem together.</h2>
      <div class="spacer" style="height: 50px;"></div>
      <a href="#" class="btn btn-green squared">Learn More</a>
    </div>
  </section>
  
@endsection