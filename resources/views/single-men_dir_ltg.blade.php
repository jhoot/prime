@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <style>
  div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1189.drts-col-12 > div {
  display: block;
  float: right;
  background: white;
  padding: 5px 10px;
  margin-bottom: 10px;
}
div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1189.drts-col-12 > div .drts-entity-field-label {
  display: inline-block;
  max-width: 40px;
  max-height: 40px;
  color: blue;
}
div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1189.drts-col-12 > div .drts-entity-field-value {
  display: inline-block;
  color: blue;
}

div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1200.drts-col-12 > div.drts-display-element.drts-display-element-1206 > div > div.drts-display-element.drts-display-element-1208.drts-col-6 > div {
  background: #2B2B2B!important; 
}


div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1200.drts-col-12 > div.drts-display-element.drts-display-element-1206 > div > div.drts-display-element.drts-display-element-1208.drts-col-6 > div, div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1200.drts-col-12 > div.drts-display-element.drts-display-element-1206 > div > div.drts-display-element.drts-display-element-1207.drts-col-6 > div {
  background: black;
  padding: 15px 10px;
  margin: 10px 0;
}
div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1200.drts-col-12 > div.drts-display-element.drts-display-element-1206 > div > div.drts-display-element.drts-display-element-1208.drts-col-6 > div .drts-entity-field-label, div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1200.drts-col-12 > div.drts-display-element.drts-display-element-1206 > div > div.drts-display-element.drts-display-element-1208.drts-col-6 > div .drts-entity-field-value, div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1200.drts-col-12 > div.drts-display-element.drts-display-element-1206 > div > div.drts-display-element.drts-display-element-1207.drts-col-6 > div .drts-entity-field-label, div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1200.drts-col-12 > div.drts-display-element.drts-display-element-1206 > div > div.drts-display-element.drts-display-element-1207.drts-col-6 > div .drts-entity-field-value {
  color: white;
  display: inline-block;
}
div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1200.drts-col-12 > div.drts-display-element.drts-display-element-1206 > div > div.drts-display-element.drts-display-element-1208.drts-col-6 > div .drts-entity-field-value, div.drts-display-element.drts-display-element-1188 > div > div.drts-display-element.drts-display-element-1200.drts-col-12 > div.drts-display-element.drts-display-element-1206 > div > div.drts-display-element.drts-display-element-1207.drts-col-6 > div .drts-entity-field-value {
  float: right;
}

.drts-display-element-header span {
  background-color: #00248B!important;
  color: white;
}
  </style>
  <?php # print_r($post); ?>
  <section class="uk-block bg5">
    <div class="gridxl">
      <div class="uk-grid uk-grid-medium">
        <div class="main uk-width-1-1 uk-width-2-3@m">
          <h2 class="color-black bold"><?php the_title(); ?></h2>
          <?php the_content(); ?>
        </div>
        <div class="sidebar uk-width-1-1 uk-width-1-3@m">

          <style>
            #singlecontact input {
              width: 100%;
              margin: 10px auto;
              display: block;
            }
            #singlecontact textarea {
              display: block;
              width: 85%;
              margin: 10px auto;
            }
            @media (max-width: 969px) {
              .sidebar {
                margin-top: 30px;
              } 
            }
            
          </style>

          <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/getaconsult.png');">
            <div class="uk-padding uk-text-center uk-text-left@m">
              <h3 class="color-white bold">Get a Consultation</h3>
              <?= do_shortcode('[contact-form-7 id="14083" title="Untitled"]'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php # the_content(); ?>

  @endwhile
@endsection