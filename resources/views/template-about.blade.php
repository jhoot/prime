{{--
  Template Name: About Template
--}}

@extends('layouts.app')

@section('content')
  <section id="aboutfold" class="bgpeach uk-block">
    <img src="/prime/wp-content/uploads/2019/01/aboutleftleaf.png" alt="" class="left">
    <img src="/prime/wp-content/uploads/2019/01/aboutrightleaf.png" alt="" class="right">
    <div class="gridl">
      <div class="image uk-text-center">
        <img src="<?php the_field('main_image'); ?>" alt="">
      </div>
      <div class="copy uk-text-left uk-padding-small">
        <h1 class="color-black bold"><?php the_field('page_header'); ?></h1>
      </div>
    </div>
  </section>
  <section id="aboutbelowfold" class="uk-block bg5">
    <div class="gridl">
      <div class="uk-grid uk-grid-large uk-text-center">
        <div class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
          <div class="uk-padding">
            <h4><?php the_field('text_left'); ?></h4>
          </div>
        </div>
        <div class="uk-width-1-1 img uk-width-1-2@m uk-text-center uk-text-left@m">
          <img src="<?php the_field('image_right'); ?>" alt="">
        </div>
        <div class="uk-width-1-1 img uk-width-1-2@m uk-text-center uk-text-left@m">
          <img src="<?php the_field('image_left'); ?>" alt="">
        </div>
        <div class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
          <div class="uk-padding">
            <h4><?php the_field('text_right'); ?></h4>
          </div>
        </div>
      </div>
      <div class="uk-padding-large grids uk-text-left">
        <h4><?php the_field('text_below'); ?></h4>
      </div>
    </div>
  </section>

  <section id="aboutpagebreak" class="uk-block bg3">
    <div class="gridm">
      <p class="color-white uk-padding">
          <?php the_field('page_break_text'); ?>
      </p>
    </div>
    <span class="left">PRIME</span>
    <span class="right">PRIME</span>
  </section>

  <section class="uk-block bg-white">
    <div class="gridl">
      <div class="uk-padding">
        <h3 class="color-black bold">View Founder's Story</h3>
      </div>
    </div>
    <div id="values" class="gridxl">
      <div class="uk-block">
        
        <div class="uk-grid uk-grid-large uk-text-center">
          <div class="valuescontainer uk-width-1-1 uk-width-1-2@s uk-width-1-6@m uk-text-left">
              <p class="bold rotated color-black">OUR VALUES</p>
          </div>
          <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-6@m uk-text-center uk-text-left@m">
            <img src="/prime/wp-content/uploads/2019/01/iconcareblack.png" alt="">
            <p class="color-black">We build social value by helping people become the best version of themselves</p>
          </div>
          <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-6@m uk-text-center uk-text-left@m">
            <img src="/prime/wp-content/uploads/2019/01/thumbblack.png" alt="">
            <p class="color-black">We build social value by helping people become the best version of themselves</p>
          </div>
          <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-6@m uk-text-center uk-text-left@m">
            <img src="/prime/wp-content/uploads/2019/01/trophyblack.png" alt="">
            <p class="color-black">We build social value by helping people become the best version of themselves</p>
          </div>
          <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-6@m uk-text-center uk-text-left@m">
            <img src="/prime/wp-content/uploads/2019/01/buildingblack.png" alt="">
            <p class="color-black">We build social value by helping people become the best version of themselves</p>
          </div>
          <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-6@m uk-text-center uk-text-left@m">
            <img src="/prime/wp-content/uploads/2019/01/arrowblack.png" alt="">
            <p class="color-black">We build social value by helping people become the best version of themselves</p>
          </div>
        </div>
      </div>      
    </div>
    <div id="staff" class="gridxl uk-block">
      <?php $targs = array(
        "post_type" => "team",
        "posts_per_page" => -1,
        "order" => "ASC"
      );

      $tquery = new WP_Query($targs);

      if($tquery->have_posts()):

      ?>
      <div class="gridl heading">
        <div class="uk-padding uk-text-center uk-text-left@s">
          <h3 class="color-black bold">The Prime Team</h3>
        </div>
      </div>
      <div class="uk-grid uk-grid-large uk-text-center uk-text-left@s">
        <?php while($tquery->have_posts()): $tquery->the_post(); ?>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-3@m">
          <div class="container">
            <img src="<?php the_field('image'); ?>" alt="">
            <div class="text bg5 uk-text-left">
              <p class="bold color-black"><?php the_field('name'); ?></p>
              <p class="color-black"><?php the_field('title'); ?></p>
            </div>
          </div>
        </div>
      <?php endwhile; wp_reset_postdata(); ?>
      </div>
    <?php endif; ?>
    </div>
    <div id="advisors" class="gridxl uk-block">
        <?php $targs = array(
          "post_type" => "advisors",
          "posts_per_page" => -1,
          "order" => "ASC"
        );
  
        $mquery = new WP_Query($margs);
  
        if($mquery->have_posts()):
  
        ?>
        <div class="gridl heading">
          <div class="uk-padding uk-text-center uk-text-left@s">
            <h3 class="color-black bold">Medical Advisory Board</h3>
          </div>
        </div>
        <div class="uk-grid uk-grid-large uk-text-center uk-text-left@s">
          <?php while($nquery->have_posts()): $mquery->the_post(); ?>
          <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-3@m">
            <div class="container">
              <img src="<?php the_field('image'); ?>" alt="">
              <div class="text bg5 uk-text-left">
                <p class="bold color-black"><?php the_field('name'); ?></p>
                <p class="color-black"><?php the_field('title'); ?></p>
              </div>
            </div>
          </div>
        <?php endwhile; wp_reset_postdata(); ?>
        </div>
      <?php endif; ?>
    </div>
  </section>

  <section class="uk-block uk-background-cover" style="background-image: url('/prime/wp-content/uploads/2019/01/talkthrough.png');">
    <div class="gridm">
      <div class="uk-block uk-text-center">
        <h2 class="color-white bold">Wanna Talk It Through?</h2>
      </div>
      <div class="container bg5 uk-padding">
        <div class="inner-container">
          <?= do_shortcode('[contact-form-7 id="63" title="talkthrough"]'); ?>
        </div>
      </div>
    </div>
  </section>
@endsection