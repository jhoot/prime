@extends('layouts.app')

@section('content')

  <?php while(have_posts()): the_post(); ?>
    {{-- <section id="top" class="bg5">
      <div class="uk-grid uk-grid-small">
        <div class="uk-width-1-1 uk-width-3-5@m">
          <div class="head bg1">
            <h3 class="color-white">New articles</h3>
          </div>
          <div id="new" class="uk-grid uk-grid-large">
            <div class="uk-width-1-1 uk-width-1-2@m">
              <div class="single">
                <img src="https://via.placeholder.com/550x250" alt="">
                <div class="meta uk-padding-small bg-white">
                  <h4 class="color-black">Is There a Vaping Epidemic?</h4>
                  <span class="color-black">4 min read</span>
                </div>
              </div>
            </div>
            <div class="uk-width-1-1 uk-width-1-2@m">
              <div class="single">
                <img src="https://via.placeholder.com/550x250" alt="">
                <div class="meta uk-padding-small bg-white">
                  <h4 class="color-black">Is There a Vaping Epidemic?</h4>
                  <span class="color-black">4 min read</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="providers uk-width-1-1 uk-width-2-5@m">
          <div class="uk-grid uk-grid-large">
            <div class="single-p uk-width-1-1 uk-width-1-2@m uk-text-center">
              <div class="bg">
                <div class="container uk-text-center">
                  <img src="/wp-content/uploads/2019/01/phys1.png" alt="">
                  <div class="text uk-text-left">
                    <h4 class="name">Joe Christopher</h4>
                    <span class="color-black">Physician: Primary Care (PCP)</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="single-p uk-width-1-1 uk-width-1-2@m uk-text-center">
              <div class="bg">
                <div class="container uk-text-center">
                  <img src="/wp-content/uploads/2019/01/phys1.png" alt="">
                  <div class="text uk-text-left">
                    <h4 class="name">Joe Christopher</h4>
                    <span class="color-black">Physician: Primary Care (PCP)</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> --}}
    <section id="article" class="uk-block">
      <div class="gridxl topmeta">
        <div class="">
          <?php 
            $tags = get_the_tags();
              foreach ($tags as $tag) {
                echo '<p class="tags">'.$tag->name.'</p>';
              }
          ?>
        </div>
        <div class="uk-block-small">
          <h2 class="color-black bold"><?php the_title(); ?></h2>
        </div>          
      </div>
      <div class="gridxl levelgrid uk-background-cover" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
        <div class="cta">
          <div class="level">
            <p class="color-white"><?= get_field('level_of_reading'); ?></p>
          </div>
          <!-- Begin Mailchimp Signup Form -->
          <div id="mc_embed_signup">
            <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
              
                  
                    
                  
                    <div id="mce-responses" class="clear">
                      <div class="response" id="mce-error-response" style="display:none"></div>
                      <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                      <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                      <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
                      <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                </div>
            </form>
          </div>
              
              <!--End mc_embed_signup-->
        </div>
      </div>
      <div class="gridxl">
        <div class="uk-grid uk-grid-medium">
          <div class="uk-width-1-1 uk-width-2-3@m main">
            <div class="post-meta">
              <div class="uk-grid uk-grid-small">
                <div class="uk-width-1-3 uk-text-center">
                  <img src="<?= get_avatar(get_the_author_ID(), 100) ? get_avatar(get_the_author_ID(), 100) : 'https://via.placeholder.com/100' ?>" alt="Author Profile Image">
                </div>
                <div class="uk-width-2-3 uk-text-left">
                  <span class="written">WROTE BY</span>
                  <p class="bold"><?php the_author(); ?></p>
                  <p><?= get_field('author_position'); ?></p>
                </div>
              </div>
            </div>
            <?php the_content(); ?>
          </div>
          <div class="uk-width-1-1 uk-width-1-3@m sidebar">
            
            <?php 

                global $wpdb;
                $results = $wpdb->get_results( "SELECT * FROM pv_drts_entity_field_entity_featured");
                $listingids = [];
                foreach ($results as $result) {
                  array_push($listingids, $result->entity_id);
                }
                shuffle($listingids);
              ?>
            <?php
              $provargs = array(
                'post_type' => 'men_dir_ltg',
                'posts_per_page' => 2,
                'order' => 'ASC',
                'orderby' => 'rand',
                'post__in'      => $listingids
              );
              $provquery = new WP_Query($provargs);
              if($provquery->have_posts()): ?>
            <div class="uk-block-small">
              <h4 class="color-black">Sponsored Providers</h4>
            </div>
            <?php
            while($provquery->have_posts()): $provquery->the_post();
            ?>
            <div class="single-p uk-width-1-1">
              <div class="bg">
                <a href="<?php the_permalink(); ?>">
                  <div class="container">
                    <style>
                    .switcherone .gridl .uk-block-small .uk-grid .content ul li .uk-grid .single-p .bg .container img {
                      left: 0!important;
                      margin-left: 0!important;
                      width: 100%;
                    }  
                    </style>
                    <img src="<?php the_field('featured_image'); ?>" style="max-width: 100%;">
                    <div class="text uk-text-left">
                      <h4 class="name"><?php the_title(); ?></h4>
                      {{-- <span class="color-black">Physician: Primary Care (PCP)</span> --}}
                    </div>
                  </div>
                </a>
              </div>
            </div>
          <?php endwhile; wp_reset_postdata();
          endif; ?>
            <div class="cta">
              <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                <div class="interior">
                  <h4 class="color-white bold">Getting Started and need to stay in your Prime</h4>


<!-- Begin Mailchimp Signup Form -->
<div id="mc_embed_signup2">
    <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll">
            <div class="mc-field-group">
                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="First Name">
              </div>
              <div class="mc-field-group">
                <input type="text" value="" name="LNAME" class="" id="mce-LNAME" placeholder="Last Name">
              </div>
    <div class="mc-field-group">
      <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
    </div>
    
    <div id="mce-responses" class="clear">
        <div class="response" id="mce-error-response" style="display:none"></div>
        <div class="response" id="mce-success-response" style="display:none"></div>
      </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
        <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
        </div>
    </form>
    </div>
    
    <!--End mc_embed_signup-->


                </div>
                
              </div>
            </div>
            <div class="uk-padding uk-text-center followus">
              <span><span>Follow us</span>  <a href="#"><img src="@asset('images/fb.svg');" /></a>  <a href="#"><img src="@asset('images/twit.svg');" /></a>  <a href="#"><img src="@asset('images/ig.svg');" /></a>  <a href="#"><img src="@asset('images/goog.svg');" /></a></span>
              
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="uk-block">
      <div class="gridxl">
        <div class="uk-block-small">
          <h2 class="color-black">Related articles you might love reading</h2>
        </div>
        <?php $args = array(
          "post_type" => "post",
          "post_per_page" => 8,
          "order" => "ASC"
          );
          $query = new WP_Query($args);
          if($query->have_posts()): ?>
            <div id="new" class="uk-grid uk-grid-small">
              <?php while($query->have_posts()): $query->the_post(); ?>
                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
                  <a href="<?php the_permalink(); ?>">   
                    <div class="single">
                      <img src="<?php the_post_thumbnail_url(); ?>">
                      <div class="meta uk-padding-small bg-white">
                        <h4 class="color-black"><?php the_title(); ?></h4>
                        {{-- <span class="color-black">4 min read</span> --}}
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>
            </div>
          <?php endif; ?>
      </div>
    </section>
    <section class="finalcta uk-block">
  <div class="gridl">
    <div class="uk-grid uk-grid-large">
      <div class="uk-width-1-1 uk-width-1-2@m">
        <img src="/wp-content/uploads/2019/01/plants-1.png" alt="">
      </div>
      <div class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
        <div class="wrap uk-block">
            <h3 class="color-black">We're committed to being the most trusted online review community on the market.</h3>
            <div id="mc_embed_signupgray">
                <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
          
                        <div id="mce-responses" class="clear">
                          <div class="response" id="mce-error-response" style="display:none"></div>
                          <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                          <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                          <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
                          <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                    </div>
                </form>
              </div>
        </div>
        
      </div>
    </div>
  </div>
</section>
<?php endwhile; ?> 
@endsection
