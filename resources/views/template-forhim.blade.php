{{--
  Template Name: For Him Template
--}}

@extends('layouts.app')

@section('content')
<section class="uk-block fold uk-background-cover" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
  <div class="gridxl" style="position: relative;">
    <div class="uk-padding uk-text-right@s followus" style="position: absolute; top: 0px; right: 20px;
    display: block;">
        <span style="color: white;"><span>Follow us</span>  <a href="https://facebook.com/keepmeprime" target="_blank"><img src="/wp-content/uploads/2019/01/Facebook.png" /></a>  <a href="#"><img src="/wp-content/uploads/2019/01/Twitter.png" /></a>  <a href="https://instagram.com/keepmeprime" target="_blank"><img src="/wp-content/uploads/2019/01/instagram.png" /></a>  <a href="#"><img src="/wp-content/uploads/2019/01/Google.png" /></a></span>
    </div>
    <h2 class="color-white bold"><?php the_field('header'); ?></h2>
    <!-- Begin Mailchimp Signup Form -->
    <div id="mc_embed_signup3">
      <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
          <div id="mc_embed_signup_scroll">

              <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
          </div>
      </form>
    </div>
  </div>
</section>

<div class="uk-block">
  <div class="gridxl">
    
      <?php 

        global $wpdb;
        $results = $wpdb->get_results( "SELECT * FROM pv_drts_entity_field_entity_featured");
        $listingids = [];
        foreach ($results as $result) {
          array_push($listingids, $result->entity_id);
        }
      ?>
      <?php
      $provargs = array(
        'post_type' => 'men_dir_ltg',
        'posts_per_page' => 3,
        'order'=> 'ASC',
        'orderby'=>'rand',
        'post__in'      => $listingids
      );
      $provquery = new WP_Query($provargs);
      if($provquery->have_posts()): ?>

        <div class="uk-block-small">
          <h4 class="color-black">Sponsored Providers</h4>
        </div>

        <div class="uk-grid uk-grid-small uk-text-center">
          
          <?php while($provquery->have_posts()): $provquery->the_post(); ?>
          <div class="single-p uk-width-1-1 uk-width-1-2@s uk-width-1-3@m">
            <div class="bg">
              <a href="<?php the_permalink(); ?>">
                <div class="container">
                  <style>
                  .switcherone .gridl .uk-block-small .uk-grid .content ul li .uk-grid .single-p .bg .container img {
                    left: 0!important;
                    margin-left: 0!important;
                    width: 100%;
                  }  
                  </style>
                  <img src="<?php the_field('featured_image'); ?>" style="max-width: 100%;">
                  <div class="text uk-text-left">
                    <h4 class="name"><?php the_title(); ?></h4>
                    {{-- <span class="color-black">Physician: Primary Care (PCP)</span> --}}
                  </div>
                </div>
              </a>
            </div>
          </div>
          <?php endwhile; wp_reset_postdata(); ?>
        </div>
      <?php endif; ?>
  </div>
</div>
<style>
.roll ul.tabsul li {
  padding: 20px!important;
}
</style>
<section class="roll uk-block-small">
    <ul class="tabsul uk-text-center uk-container-center bg5" uk-switcher>
      <li><a href="#">Recent</a></li>
      <li><a href="#">GUT</a></li>
      <li><a href="#">Brain</a></li>
      <li><a href="#">Longevity</a></li>
      <li><a href="#">Hormones</a></li>
      <li><a href="#">Emotions</a></li>
      <li><a href="#">Mindset</a></li>
      <li><a href="#">Testing</a></li>
      <li><a href="#">Products</a></li>
    </ul>
    <ul class="uk-switcher">
      <li>
        <section class="uk-block bg-white">
          <div class="gridxl">
            <?php $args = array(
              'post_type' => 'post',
              'post_per_page' => 40,
              'order' => 'ASC'
            );
            
            $query = new WP_Query($args);
            
            if($query->have_posts()):
            
            ?>
            <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid="masonry: true">
              <?php while($query->have_posts()): $query->the_post(); ?>
              <div class="item">
                <?php $category = get_the_category();
                $firstCategory = $category[0]->cat_name; ?>
                <a href="<?php the_permalink(); ?>">
                  <div class="container">
                    <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                    <div class="text">
                      <p class="cat"><?= $firstCategory; ?></p>
                      <h4 class="color-black"><?php the_title(); ?></h4>
                      <p class="excerpt"><?= wp_strip_all_tags( get_the_excerpt() ); ?></p>
                    </div>
                  </div>
                </a>
              </div>
              
            <?php endwhile; ?>
            
              <div class="item">
                <div class="cta">
                  <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                    <div class="interior">
                      <h4 class="color-white bold">Still need help and want personalized guidance via phone from a Prime expert.</h4>
                        <!-- Begin Mailchimp Signup Form -->
                        <div id="mc_embed_signup10">
                          <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                              <div id="mc_embed_signup_scroll">
                                  <div class="mc-field-group">
                                      <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your full Name">
                                    </div>
                                    <div class="mc-field-group">
                                      <input type="email" value="" name="EMAIL" class="" id="mce-EMAIL" placeholder="Email Address">
                                    </div>
                            <div class="mc-field-group">
                              <input type="text" value="" name="TEL" class="required tel" id="mce-TEL" placeholder="Telephone Number">
                            </div>
                          
                            <div id="mce-responses" class="clear">
                              <div class="response" id="mce-error-response" style="display:none"></div>
                              <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                              <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                              <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                              </div>
                          </form>
                        </div>
                        <!--End mc_embed_signup-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; wp_reset_postdata(); ?>
          </div>
        </section>
      </li>
      <li>
        <section class="uk-block bg-white">
          <div class="gridxl">
            <?php $gutargs = array(
              'post_type' => 'post',
              'post_per_page' => 40,
              'order' => 'ASC',
              'category_name' => 'gut'
            );
            
            $gutquery = new WP_Query($gutargs);
            if($gutquery->have_posts()): 
            ?>
            <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid="masonry: true">
                <?php while($gutquery->have_posts()): $gutquery->the_post(); ?>
                <div class="item">
                  <?php $category = get_the_category();
                  $firstCategory = $category[0]->cat_name; ?>
                  <a href="<?php the_permalink(); ?>">
                    <div class="container">
                      <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                      <div class="text">
                        <p class="cat"><?= $firstCategory; ?></p>
                        <h4 class="color-black"><?php the_title(); ?></h4>
                        <p class="excerpt"><?= wp_strip_all_tags( get_the_excerpt() ); ?></p>
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>

              <div class="item">
                  <div class="cta">
                    <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                      <div class="interior">
                        <h4 class="color-white bold">Still need help and want personalized guidance via phone from a Prime expert.</h4>
                          <!-- Begin Mailchimp Signup Form -->
                          <div id="mc_embed_signup10">
                            <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your full Name">
                                      </div>
                                      <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="" id="mce-EMAIL" placeholder="Email Address">
                                      </div>
                              <div class="mc-field-group">
                                <input type="text" value="" name="TEL" class="required tel" id="mce-TEL" placeholder="Telephone Number">
                              </div>
                            
                              <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </div>
                            </form>
                          </div>
                          <!--End mc_embed_signup-->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          </div>
        </section>
      </li>
      <li>
        <section class="uk-block bg-white">
          <div class="gridxl">
            <?php $brainargs = array(
              'post_type' => 'post',
              'post_per_page' => 40,
              'order' => 'ASC',
              'category_name' => 'brain'
            );
            
            $brainquery = new WP_Query($brainargs);

            if($brainquery->have_posts()): 
            ?>
            <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid="masonry: true">
                <?php while($brainquery->have_posts()): $brainquery->the_post(); ?>
                <div class="item">
                  <?php $category = get_the_category();
                  $firstCategory = $category[0]->cat_name; ?>
                  <a href="<?php the_permalink(); ?>">
                    <div class="container">
                      <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                      <div class="text">
                        <p class="cat"><?= $firstCategory; ?></p>
                        <h4 class="color-black"><?php the_title(); ?></h4>
                        <p class="excerpt"><?= wp_strip_all_tags( get_the_excerpt() ); ?></p>
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>

              <div class="item">
                  <div class="cta">
                    <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                      <div class="interior">
                        <h4 class="color-white bold">Still need help and want personalized guidance via phone from a Prime expert.</h4>
                          <!-- Begin Mailchimp Signup Form -->
                          <div id="mc_embed_signup10">
                            <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your full Name">
                                      </div>
                                      <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="" id="mce-EMAIL" placeholder="Email Address">
                                      </div>
                              <div class="mc-field-group">
                                <input type="text" value="" name="TEL" class="required tel" id="mce-TEL" placeholder="Telephone Number">
                              </div>
                            
                              <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </div>
                            </form>
                          </div>
                          <!--End mc_embed_signup-->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          </div>
        </section>
      </li>
      <li>
        <section class="uk-block bg-white">
          <div class="gridxl">
            <?php $longargs = array(
              'post_type' => 'post',
              'post_per_page' => 40,
              'order' => 'ASC',
              'category_name' => 'longevity'
            );
            
            $longquery = new WP_Query($longargs);

            if($longquery->have_posts()):  
            ?>
            <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid="masonry: true">
                <?php while($longquery->have_posts()): $longquery->the_post(); ?>
                <div class="item">
                  <?php $category = get_the_category();
                  $firstCategory = $category[0]->cat_name; ?>
                  <a href="<?php the_permalink(); ?>">
                    <div class="container">
                      <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                      <div class="text">
                        <p class="cat"><?= $firstCategory; ?></p>
                        <h4 class="color-black"><?php the_title(); ?></h4>
                        <p class="excerpt"><?= wp_strip_all_tags( get_the_excerpt() ); ?></p>
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>

              <div class="item">
                  <div class="cta">
                    <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                      <div class="interior">
                        <h4 class="color-white bold">Still need help and want personalized guidance via phone from a Prime expert.</h4>
                          <!-- Begin Mailchimp Signup Form -->
                          <div id="mc_embed_signup10">
                            <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your full Name">
                                      </div>
                                      <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="" id="mce-EMAIL" placeholder="Email Address">
                                      </div>
                              <div class="mc-field-group">
                                <input type="text" value="" name="TEL" class="required tel" id="mce-TEL" placeholder="Telephone Number">
                              </div>
                            
                              <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </div>
                            </form>
                          </div>
                          <!--End mc_embed_signup-->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          </div>
        </section>
      </li>
      <li>
        <section class="uk-block bg-white">
          <div class="gridxl">
            <?php $hormoneargs = array(
              'post_type' => 'post',
              'post_per_page' => 40,
              'order' => 'ASC',
              'category_name' => 'hormones'
            );
            
            $hormonequery = new WP_Query($hormoneargs);

            if($hormonequery->have_posts()):
            ?>
            <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid="masonry: true">
                <?php while($hormonequery->have_posts()): $hormonequery->the_post(); ?>
                <div class="item">
                  <?php $category = get_the_category();
                  $firstCategory = $category[0]->cat_name; ?>
                  <a href="<?php the_permalink(); ?>">
                    <div class="container">
                      <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                      <div class="text">
                        <p class="cat"><?= $firstCategory; ?></p>
                        <h4 class="color-black"><?php the_title(); ?></h4>
                        <p class="excerpt"><?= wp_strip_all_tags( get_the_excerpt() ); ?></p>
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>

              <div class="item">
                  <div class="cta">
                    <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                      <div class="interior">
                        <h4 class="color-white bold">Still need help and want personalized guidance via phone from a Prime expert.</h4>
                          <!-- Begin Mailchimp Signup Form -->
                          <div id="mc_embed_signup10">
                            <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your full Name">
                                      </div>
                                      <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="" id="mce-EMAIL" placeholder="Email Address">
                                      </div>
                              <div class="mc-field-group">
                                <input type="text" value="" name="TEL" class="required tel" id="mce-TEL" placeholder="Telephone Number">
                              </div>
                            
                              <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </div>
                            </form>
                          </div>
                          <!--End mc_embed_signup-->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          </div>
        </section>
      </li>
      <li>
        <section class="uk-block bg-white">
          <div class="gridxl">
            <?php $emotionargs = array(
              'post_type' => 'post',
              'post_per_page' => 40,
              'order' => 'ASC',
              'category_name' => 'emotions'
            );
            
            $emotionquery = new WP_Query($emotionargs);
            if($emotionquery->have_posts()):
            ?>
            <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid="masonry: true">
              <?php while($emotionquery->have_posts()): $emotionquery->the_post(); ?>
                <div class="item">
                  <?php $category = get_the_category();
                  $firstCategory = $category[0]->cat_name; ?>
                  <a href="<?php the_permalink(); ?>">
                    <div class="container">
                      <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                      <div class="text">
                        <p class="cat"><?= $firstCategory; ?></p>
                        <h4 class="color-black"><?php the_title(); ?></h4>
                        <p class="excerpt"><?= wp_strip_all_tags( get_the_excerpt() ); ?></p>
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>

              <div class="item">
                  <div class="cta">
                    <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                      <div class="interior">
                        <h4 class="color-white bold">Still need help and want personalized guidance via phone from a Prime expert.</h4>
                          <!-- Begin Mailchimp Signup Form -->
                          <div id="mc_embed_signup10">
                            <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your full Name">
                                      </div>
                                      <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="" id="mce-EMAIL" placeholder="Email Address">
                                      </div>
                              <div class="mc-field-group">
                                <input type="text" value="" name="TEL" class="required tel" id="mce-TEL" placeholder="Telephone Number">
                              </div>
                            
                              <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </div>
                            </form>
                          </div>
                          <!--End mc_embed_signup-->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          </div>
        </section>
      </li>
      <li>
        <section class="uk-block bg-white">
          <div class="gridxl">
            <?php $mindargs = array(
              'post_type' => 'post',
              'post_per_page' => 40,
              'order' => 'ASC',
              'category_name' => 'mindset'
            );
            
            $mindquery = new WP_Query($mindargs);
            if($mindquery->have_posts()):  
            ?>
            <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid="masonry: true">
              <?php while($mindquery->have_posts()): $mindquery->the_post(); ?>
                <div class="item">
                  <?php $category = get_the_category();
                  $firstCategory = $category[0]->cat_name; ?>
                  <a href="<?php the_permalink(); ?>">
                    <div class="container">
                      <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                      <div class="text">
                        <p class="cat"><?= $firstCategory; ?></p>
                        <h4 class="color-black"><?php the_title(); ?></h4>
                        <p class="excerpt"><?= wp_strip_all_tags( get_the_excerpt() ); ?></p>
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>

              <div class="item">
                  <div class="cta">
                    <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                      <div class="interior">
                        <h4 class="color-white bold">Still need help and want personalized guidance via phone from a Prime expert.</h4>
                          <!-- Begin Mailchimp Signup Form -->
                          <div id="mc_embed_signup10">
                            <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your full Name">
                                      </div>
                                      <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="" id="mce-EMAIL" placeholder="Email Address">
                                      </div>
                              <div class="mc-field-group">
                                <input type="text" value="" name="TEL" class="required tel" id="mce-TEL" placeholder="Telephone Number">
                              </div>
                            
                              <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </div>
                            </form>
                          </div>
                          <!--End mc_embed_signup-->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          </div>
        </section>
      </li>
      <li>
        <section class="uk-block bg-white">
          <div class="gridxl">
            <?php $testargs = array(
              'post_type' => 'post',
              'post_per_page' => 40,
              'order' => 'ASC',
              'category_name' => 'testing'
            );
            
            $testquery = new WP_Query($testargs);
            if($testquery->have_posts()):
            ?>
            <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid="masonry: true">
                <?php while($testquery->have_posts()): $testquery->the_post(); ?>
                <div class="item">
                  <?php $category = get_the_category();
                  $firstCategory = $category[0]->cat_name; ?>
                  <a href="<?php the_permalink(); ?>">
                    <div class="container">
                      <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                      <div class="text">
                        <p class="cat"><?= $firstCategory; ?></p>
                        <h4 class="color-black"><?php the_title(); ?></h4>
                        <p class="excerpt"><?= wp_strip_all_tags( get_the_excerpt() ); ?></p>
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>

              <div class="item">
                  <div class="cta">
                    <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                      <div class="interior">
                        <h4 class="color-white bold">Still need help and want personalized guidance via phone from a Prime expert.</h4>
                          <!-- Begin Mailchimp Signup Form -->
                          <div id="mc_embed_signup10">
                            <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your full Name">
                                      </div>
                                      <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="" id="mce-EMAIL" placeholder="Email Address">
                                      </div>
                              <div class="mc-field-group">
                                <input type="text" value="" name="TEL" class="required tel" id="mce-TEL" placeholder="Telephone Number">
                              </div>
                            
                              <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </div>
                            </form>
                          </div>
                          <!--End mc_embed_signup-->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          </div>
        </section>
      </li>
      <li>
        <section class="uk-block bg-white">
          <div class="gridxl">
            <?php $productargs = array(
              'post_type' => 'post',
              'post_per_page' => 40,
              'order' => 'ASC',
              'category_name' => 'products'
            );
            
            $productquery = new WP_Query($productargs);
            if($productquery->have_posts()):  
            ?>
            <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid="masonry: true">
              <?php while($productquery->have_posts()): $productquery->the_post(); ?>
                <div class="item">
                  <?php $category = get_the_category();
                  $firstCategory = $category[0]->cat_name; ?>
                  <a href="<?php the_permalink(); ?>">
                    <div class="container">
                      <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                      <div class="text">
                        <p class="cat"><?= $firstCategory; ?></p>
                        <h4 class="color-black"><?php the_title(); ?></h4>
                        <p class="excerpt"><?= wp_strip_all_tags( get_the_excerpt() ); ?></p>
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>

              <div class="item">
                  <div class="cta">
                    <div class="container uk-background-cover" style="background-image: url('/wp-content/uploads/2019/01/bluevert-1.png');">
                      <div class="interior">
                        <h4 class="color-white bold">Still need help and want personalized guidance via phone from a Prime expert.</h4>
                          <!-- Begin Mailchimp Signup Form -->
                          <div id="mc_embed_signup10">
                            <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your full Name">
                                      </div>
                                      <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="" id="mce-EMAIL" placeholder="Email Address">
                                      </div>
                              <div class="mc-field-group">
                                <input type="text" value="" name="TEL" class="required tel" id="mce-TEL" placeholder="Telephone Number">
                              </div>
                            
                              <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Register Myself Now" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </div>
                            </form>
                          </div>
                          <!--End mc_embed_signup-->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          </div>
        </section>
      </li> 
    </ul>
</section>
<section class="uk-block-small">
  <div class="gridxl">
    <div class="uk-padding uk-text-center uk-text-right@s followus">
        <span><span>Follow us</span>  <a href="#"><img src="@asset('images/fb.svg');" /></a>  <a href="#"><img src="@asset('images/twit.svg');" /></a>  <a href="#"><img src="@asset('images/ig.svg');" /></a>  <a href="#"><img src="@asset('images/goog.svg');" /></a></span>
    </div>
  </div>

</section>

{{-- Recent GUT Brain Longevity Hormones Emotions Mindset Testing Products --}}


@endsection