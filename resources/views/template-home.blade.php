{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
<section id="fold" class="uk-background-cover" style="background-image: url('/prime/wp-content/uploads/2019/01/homebgv2.png'); background-position: 50%;">
  <div class=" gridm uk-text-center">
    <div class="uk-grid uk-grid-collapse">
      <div class="uk-width-1-1 uk-text-center">
        <div class="aging">
          <h1 class="color-black bold"><?php the_field('header'); ?></h1>
          <h4 class="color-black"><?php the_field('sub_header'); ?></h4>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="emailcapture bg-black uk-block">
  <div class="grids uk-text-center">
    <p class="color-white"><?php the_field('page_break_copy'); ?></p>
    <div class="uk-grid uk-grid-medium">
      <div class="uk-width-1-1 uk-width-1-3@s">
        <span class="bold live">LIVE LONGER.</span>
      </div>
      <div class="uk-width-1-1 uk-width-1-3@s">
        <span class="bold get">GET STRONGER.</span>
      </div>
      <div class="uk-width-1-1 uk-width-1-3@s">
        <span class="bold love">LOVE MORE.</span>
      </div>
    </div>
  </div>
</div>

<section class="left-text-block uk-block bg-white">
  <div class=" gridl">
    <div class="uk-grid uk-grid-large">
      <div class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
        <div class="wrap uk-block">
            <h4 class="color-black bold"><?php the_field('header_one'); ?></h4>
            <br>
            <p class="color-black ntm" style="margin-top: 5px;"><?php the_field('copy_one'); ?></p>
    
            <div id="mc_embed_signupgray">
              <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                  <div id="mc_embed_signup_scroll">
        
                      <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                      </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                  </div>
              </form>
            </div>
        </div>
        
      </div>
      <div class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-right@m">
        <img src="<?php the_field('image_one'); ?>" alt="">
      </div>
    </div>
  </div>
</section>

<section class="videoblock bg4 uk-block-small">
  <div class="gridxl">
    <div class="uk-grid uk-grid-collapse uk-grid-match">
      <div class="bp uk-width-1-1 uk-width-1-3@m uk-text-center uk-text-left@m uk-text-center uk-text-left@m">
        <div class="blogpost">
          <img src="<?php the_field('blurb_one_image'); ?>" alt="">
          <div class="text bg-white">
            <h4 class=""><?php the_field('blurb_one_copy'); ?></h4>
            <br>
            <br>
            <a href="<?php the_field('blurb_one_link'); ?>" class="bold color3">Find Now <span uk-icon="triangle-right"></span></a>
          </div>
        </div>
      </div>
      <div class="bp uk-width-1-1 uk-width-1-3@m uk-text-center uk-text-left@m">
        <div class="blogpost">
          <img src="<?php the_field('blurb_two_image'); ?>" alt="">
          <div class="text bg-white">
            <h4 class=""><?php the_field('blurb_two_copy'); ?></h4>
            <br>
            <br>
            <a href="<?php the_field('blurb_two_link'); ?>" class="bold color3">Find Now <span uk-icon="triangle-right"></span></a>
          </div>
        </div>
      </div>
      <div class="bp uk-width-1-1 uk-width-1-3@m uk-text-center uk-text-left@m">
        <div class="blogpost">
          <img src="/prime/wp-content/uploads/2019/01/watchvid.png" alt="">
          <div class="text bg3 uk-text-center">
            <a href="<?php the_field('blurb_three_link'); ?>">
              <h4 class="title color-white" style="margin-top: 40px!important;">Watch Our Video</h4>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="uk-block-small bg4 switcherone"> 
  <div class=" gridl">
    <div class="uk-grid uk-grid-collapse">
      <div class="uk-width-1-1 uk-width-5-6@m">
        <div class="uk-block-small">
          <h3 class="color-black"><?php the_field('header_two'); ?></h3>
        </div>
      </div>
    </div>
    <div class="uk-block-small">
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-width-1-4@m nav">
          <ul uk-tab="connect: #switchcontent">
            <li class="color-black"><a href="#">Providers</a></li>
            <li class="color-black"><a href="#">Providers</a></li>
            <li class="color-black"><a href="#">Providers</a></li>
            <li class="color-black"><a href="#">Providers</a></li>
          </ul>
        </div>
        <div class="uk-width-1-1 uk-width-3-4@m content">
          <ul id="switchcontent" class="uk-switcher">
            <li>
              <div class="uk-grid uk-grid-small">
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="uk-grid uk-grid-small">
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="uk-grid uk-grid-small">
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="uk-grid uk-grid-small">
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="single-p uk-width-1-1 uk-width-1-2@m">
                  <div class="bg">
                    <div class="container">
                      <img src="/prime/wp-content/uploads/2019/01/phys1.png" alt="">
                      <div class="text uk-text-left">
                        <h4 class="name">Joe Christopher</h4>
                        <span class="color-black">Physician: Primary Care (PCP)</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>
          <a href="#" class="btn btn-black squared">See More Physicians</a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="uk-block bg-white switchertwo">
  <div class=" gridl">
    <div class="uk-grid uk-grid-collapse">
      <div class="uk-width-1-1 uk-width-1-4@m nav">
        <ul uk-tab="connect: #switchcontent2">
          <li class="color-black"><a href="#">Providers</a></li>
          <li class="color-black"><a href="#">Lifestyle</a></li>
          <li class="color-black"><a href="#">Product Review</a></li>
          <li class="color-black"><a href="#">Product Stores</a></li>
        </ul>
      </div>
      <div class="uk-width-1-1 uk-width-3-4@m content">
        <ul class="uk-switcher uk-text-center" id="switchcontent2">
          <li>
            <div class="uk-grid uk-grid-small">
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto</span>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="uk-grid uk-grid-small">
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="uk-grid uk-grid-small">
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="uk-grid uk-grid-small">
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
              <div class="single-2 uk-width-1-1 uk-width-1-2@m">
                <div class="container">
                  <img src="/prime/wp-content/uploads/2019/01/forty.png" alt="">
                  <span class="bg-white">Guide to Keto2</span>
                </div>
              </div>
            </div>
          </li>
        </ul>
        
      </div>
      
    </div>
    <a href="#" class="btn btn-black squared">Explore More</a>
  </div>
</section>


<section class="product-review bg5 uk-block">
  <div class=" gridl">
    <div class="prheader uk-text-center">
      <img src="/prime/wp-content/uploads/2019/01/stars.png" alt="">
      <h3 style="margin-top: 10px;">Product Review</h3>
      <p>We're committed to being the most trusted online review community on the market.</p>
    </div>  
    <ul uk-switcher="animation: uk-animation-fade" class="uk-grid uk-text-center">
      <li>
        <div class="uk-width-1-2 uk-width-1-4@m">
          <div class="circle active-circle">
            <span>Vie Light</span>
          </div>
        </div>
      </li>
      <li>
        <div class="uk-width-1-2 uk-width-1-4@m">
          <div class="circle">
            <span>Vie Light</span>
          </div>
        </div>
      </li>
      <li>
        <div class="uk-width-1-2 uk-width-1-4@m">
          <div class="circle">
            <span>Vie Light</span>
          </div>
        </div>
      </li>
      <li>
        <div class="uk-width-1-2 uk-width-1-4@m">
          <div class="circle">
            <span>Vie Light</span>
          </div>
        </div>
      </li>
    </ul>
    <ul class="uk-switcher">
      <li>
        <div class="single-product uk-block">
          <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-1-1 uk-width-1-3@s border">
              <img src="/prime/wp-content/uploads/2019/01/head.png" alt="">
            </div>
            <div class="uk-width-1-1 uk-width-2-3@s text">
              <div class="container">
                <h4 class="color-black">Vie Light</h4>
                <p class="color-black">Lorem ipsum dolor sit amet, in eligendi suscipit lucilius eam, diam reprehendunt ad qui. Vix ad veniam senserit consectetuer, mei nemore verear an. Ne meis eloquentiam eum, ne lorem populo perpetua nam. Purto nihil iuvaret nec cu. Oblique mandamus ex pro, et dico aeque facilisi usu.</p>
                <span class="color-black"><span class="bold">16</span> Positive Reviews</span>
                <a href="#" class="btn btn-black squared  ">Learn More</a>
              </div>
            </div>
          </div>
        </div>
      </li>
      <li>
        <div class="single-product uk-block">
          <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-1-1 uk-width-1-3@s border">
              <img src="/prime/wp-content/uploads/2019/01/head.png" alt="">
            </div>
            <div class="uk-width-1-1 uk-width-2-3@s text">
              <div class="container">
                <h4 class="color-black">Vie Light</h4>
                <p class="color-black">Lorem ipsum dolor sit amet, in eligendi suscipit lucilius eam, diam reprehendunt ad qui. Vix ad veniam senserit consectetuer, mei nemore verear an. Ne meis eloquentiam eum, ne lorem populo perpetua nam. Purto nihil iuvaret nec cu. Oblique mandamus ex pro, et dico aeque facilisi usu.</p>
                <span class="color-black"><span class="bold">16</span> Positive Reviews</span>
                <a href="#" class="btn btn-black squared  ">Learn More</a>
              </div>
            </div>
          </div>
        </div>
      </li>
      <li>
        <div class="single-product uk-block">
          <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-1-1 uk-width-1-3@s border">
              <img src="/prime/wp-content/uploads/2019/01/head.png" alt="">
            </div>
            <div class="uk-width-1-1 uk-width-2-3@s text">
              <div class="container">
                <h4 class="color-black">Vie Light</h4>
                <p class="color-black">Lorem ipsum dolor sit amet, in eligendi suscipit lucilius eam, diam reprehendunt ad qui. Vix ad veniam senserit consectetuer, mei nemore verear an. Ne meis eloquentiam eum, ne lorem populo perpetua nam. Purto nihil iuvaret nec cu. Oblique mandamus ex pro, et dico aeque facilisi usu.</p>
                <span class="color-black"><span class="bold">16</span> Positive Reviews</span>
                <a href="#" class="btn btn-black squared  ">Learn More</a>
              </div>
            </div>
          </div>
        </div>
      </li>
      <li>
        <div class="single-product uk-block">
          <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-1-1 uk-width-1-3@s border">
              <img src="/prime/wp-content/uploads/2019/01/head.png" alt="">
            </div>
            <div class="uk-width-1-1 uk-width-2-3@s text">
              <div class="container">
                <h4 class="color-black">Vie Light</h4>
                <p class="color-black">Lorem ipsum dolor sit amet, in eligendi suscipit lucilius eam, diam reprehendunt ad qui. Vix ad veniam senserit consectetuer, mei nemore verear an. Ne meis eloquentiam eum, ne lorem populo perpetua nam. Purto nihil iuvaret nec cu. Oblique mandamus ex pro, et dico aeque facilisi usu.</p>
                <span class="color-black"><span class="bold">16</span> Positive Reviews</span>
                <a href="#" class="btn btn-black squared  ">Learn More</a>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</section>

{{-- <section class="uk-block-large bg3">
  <div class="gridl uk-text-center">
    <h2 class="color2 bold">Bringing all of the different parts of ecosystem together.</h2>
    <div class="spacer" style="height: 50px;"></div>
    <a href="#" class="btn btn-green squared">Learn More</a>
  </div>
</section> --}}

<section class="stack uk-block uk-text-center bg7">
  <div class="gridl">
    <img src="/prime/wp-content/uploads/2019/01/quote.png" alt="">
    <h2 class="color-white">My Stack</h2>
    <div class="spacer" style="height: 30px;"></div>
    <div class="uk-grid images uk-grid-collapse">
      <div class="uk-width-1-1 uk-width-1-3@m" id="left">
        <img src="/prime/wp-content/uploads/2019/01/prof2.png" alt="" class="img1">
        <img src="/prime/wp-content/uploads/2019/01/prof5.png" alt="" class="img2">
      </div>
      <div class="uk-width-1-1 uk-width-1-3@m">
        <img src="/prime/wp-content/uploads/2019/01/prof1.png" alt="">
      </div>
      <div class="uk-width-1-1 uk-width-1-3@m" id="right">
        <img src="/prime/wp-content/uploads/2019/01/prof4.png" alt="" class="img1">
        <img src="/prime/wp-content/uploads/2019/01/prof5.png" alt="" class="img2">
      </div>
    </div>
    <div class="gridxs uk-block-small">
      <p class="color-white large">Lorem ipsum dolor sit amet, in eligendi suscipit lucilius eam, diam reprehendunt ad qui. Vix ad veniam senserit consectetuer, mei nemore verear an. Ne meis eloquentiam eum, ne lorem populo perpetua nam.</p>
    </div>
    <div class="uk-grid names uk-grid-collapse">
      <div class="uk-width-1-1 uk-width-1-5@m one">
        <p class="nonactive">Maz Bo</p>
      </div>
      <div class="uk-width-1-1 uk-width-1-5@m two">
        <p class="nonactive">Sergil</p>
      </div>
      <div class="uk-width-1-1 uk-width-1-5@m three">
        <p class="active bold">Michael Bay</p>
      </div>
      <div class="uk-width-1-1 uk-width-1-5@m four">
        <p class="nonactive">Ibby Kay</p>
      </div>
      <div class="uk-width-1-1 uk-width-1-5@m five">
        <p class="nonactive">Henry R</p>
      </div>
    </div>
  </div>
</section>

<section class="finalcta uk-block">
  <div class="gridl">
    <div class="uk-grid uk-grid-large">
      <div class="uk-width-1-1 uk-width-1-2@m">
        <img src="<?php the_field('cta_image'); ?>" alt="">
      </div>
      <div class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
        <div class="wrap uk-block">
            <h3 class="color-black"><?php the_field('cta_copy'); ?></h3>
            <div id="mc_embed_signupgray">
                <form action="https://Keepmeprime.us19.list-manage.com/subscribe/post?u=1ceadb606d4fcf5e74218e69a&amp;id=078125da80" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
          
                        <div id="mce-responses" class="clear">
                          <div class="response" id="mce-error-response" style="display:none"></div>
                          <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                          <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ceadb606d4fcf5e74218e69a_078125da80" tabindex="-1" value=""></div>
                          <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
                          <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                    </div>
                </form>
              </div>
        </div>
        
      </div>
    </div>
  </div>
</section>
@endsection
