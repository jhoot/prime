{{--
  Template Name: Stacks Template
--}}

@extends('layouts.app')

@section('content')
<style>
#stackfold {
  min-height: 80vh;
  padding-bottom: 200px;
}
#stackfold .h1 {
  padding-top: 100px;
}

.stackcontent div.nav {
  background: #3C4F65;
}
.stackcontent div.nav ul.uk-grid {
  text-align: center;
}
.stackcontent div.nav ul.uk-grid li {
  padding: 40px;
  transition: 0.25s all ease-in-out;
}
.stackcontent div.nav ul.uk-grid li a {
  color: white;
  text-decoration: none;
  letter-spacing: 0.05em;
}
.stackcontent div.nav ul.uk-grid li.uk-active {
  background: #FF5733;
}
.stackcontent div.nav ul.uk-grid li.uk-active a {
  font-weight: bold;
}
.stackcontent div.nav ul.uk-grid li:hover a {
  text-decoration: none;
}

.bgcream {
  background: #FFF5EF;
}

.color-orange {
  color: #FF8162;
}

.color-teal {
  color: #41AAA8;
}

.bgteal {
  background: #E8F6F6;
}

.bgorange {
  background: #FBECE2;
}

.steps .gridxs .uk-padding .uk-grid .uk-width-1-6 {
  margin-top: 5px;
}
.steps .gridxs .uk-padding .uk-grid .uk-width-1-6 .stepcircle {
  background: #007AFF;
  margin-top: 10px;
  padding: 13px 15px;
  text-align: center;
  border-radius: 35px;
}
.steps .gridxs .uk-padding .uk-grid .uk-width-1-6 .stepcircle span {
  font-size: 125%;
}
.steps .gridxs .uk-padding .uk-grid .uk-width-5-6 {
  margin-bottom: 15px;
}

.whatifelt .gridl .uk-grid .uk-width-1-1 img {
  max-width: 80%;
}

.current .gridxl, .adding .gridxl {
  position: relative;
}
.current .gridxl .lefttext, .adding .gridxl .lefttext {
  position: absolute;
  left: -60px;
  top: 20px;
}
.current .gridxl .lefttext h3, .adding .gridxl .lefttext h3 {
  text-transform: uppercase;
  letter-spacing: 1.5px;
}
.current .gridxl .uk-grid .uk-width-1-1 .bgorange h4, .current .gridxl .uk-grid .uk-width-1-1 .bgteal h4, .adding .gridxl .uk-grid .uk-width-1-1 .bgorange h4, .adding .gridxl .uk-grid .uk-width-1-1 .bgteal h4 {
  font-size: 160% !important;
}
.current .gridxl .uk-grid .uk-width-1-1 .bgorange ul, .current .gridxl .uk-grid .uk-width-1-1 .bgteal ul, .adding .gridxl .uk-grid .uk-width-1-1 .bgorange ul, .adding .gridxl .uk-grid .uk-width-1-1 .bgteal ul {
  list-style-type: none;
  padding-left: 0;
}
.current .gridxl .uk-grid .uk-width-1-1 .bgorange ul li, .current .gridxl .uk-grid .uk-width-1-1 .bgteal ul li, .adding .gridxl .uk-grid .uk-width-1-1 .bgorange ul li, .adding .gridxl .uk-grid .uk-width-1-1 .bgteal ul li {
  padding: 10px 5px;
}

.adding {
  padding-bottom: 200px;
}

.limec {
  color: #6EFFBF;
}

.keypoints {
  margin-top: -75px;
}
.keypoints .uk-grid .uk-width-1-1 .bg3 {
  width: 150px;
  height: 150px;
  margin: 0 auto;
  text-align: center;
  border-radius: 150px;
}
.keypoints .uk-grid .uk-width-1-1 .bg3 h4 {
  padding-top: 50px;
}

.testresults {
  margin-top: -75px;
}
.testresults .gridm .uk-block h2 {
  margin-bottom: 20px;
}
.testresults .gridm .uk-block a.btn.squared.lime {
  background: #6EFFBF;
  padding: 10px 25px;
  color: black;
}

@media (max-width: 1500px) {
  .current .gridxl .lefttext, .adding .gridxl .lefttext {
    left: -40px;
  }
  .current .gridxl .uk-grid, .adding .gridxl .uk-grid {
    margin-right: -40px;
  }
}
@media (max-width: 1400px) {
  .current .gridxl .lefttext, .adding .gridxl .lefttext {
    top: -40px;
    left: 0;
    width: 100%;
    text-align: center;
  }
  .current .gridxl .lefttext h3, .adding .gridxl .lefttext h3 {
    transform: none !important;
    writing-mode: initial;
  }
  .current .gridxl .uk-grid, .adding .gridxl .uk-grid {
    margin-right: 0;
  }
  .current .gridxl .uk-grid .uk-width-1-1, .adding .gridxl .uk-grid .uk-width-1-1 {
    margin: 10px 0;
  }
}
@media (max-width: 959px) {
  .uk-grid div {
    margin: 10px auto;
  }
}
@media (max-width: 639px) {
  .uk-grid {
    margin-left: 0;
  }
  .uk-grid div {
    padding-left: 0;
    margin: 10px auto;
  }

  .uk-grid-medium {
    margin-left: 0;
  }
}
</style>
  <section id="stackfold" class="uk-block uk-background-cover" style="background-image: url('/prime/wp-content/uploads/2019/01/stacksbg.png');">
    <div class="gridxl h1 uk-text-center">
      <h1 class="color-black bold underlined">My stack</h1>
    </div>
    <div class="gridxl img">
      <img src="/prime/wp-content/uploads/2019/01/stacks.png">
    </div>
    <div class="grids p uk-text-center">
      <p class="color-black" style="font-size: 125%;">You deserve to stay in your Prime, maybe catch a second wind, and have a real chance to become a better version of yourself.</p>
    </div>
  </section>

  <section class="stackcontent">
    <div class="nav">
      <ul class="uk-child-width-1-1 uk-text-center uk-container-center uk-child-width-1-3@s uk-child-width-1-5@m uk-grid" uk-switcher="connect: .switchercontent">
        <li>
          <a href="#">Saad Alam</a>
        </li>
        <li>
          <a href="#">Saad Alam2</a>
        </li>
        <li>
          <a href="#">Saad Alam3</a>
        </li>
        <li>
          <a href="#">Saad Alam4</a>
        </li>
        <li>
          <a href="#">Saad Alam5</a>
        </li>
      </ul>
    </div>
    <div class="bg5">
        <ul class="uk-switcher switchercontent">
          <li>
            <div class="gridm bg-white">
              <div class="uk-padding-large">
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-2-3@s">
                    <div class="uk-grid uk-grid-large uk-text-center uk-container-center">
                      <div class="uk-width-1-1 uk-width-1-2@s profimg uk-text-center uk-text-right@s">
                        <img src="/prime/wp-content/uploads/2019/01/saadprofile.png" style="max-width: 200px;">
                      </div>
                      <div class="uk-width-1-1 uk-width-1-2@s profbio uk-text-center uk-text-left@s">
                        <div class="uk-block-small">
                          <span class="color-black"><span class="bold">Name:</span> Saad Alam</span><br><br>
                          <span class="color-black"><span class="bold">Name:</span> Saad Alam</span><br><br>
                          <span class="color-black"><span class="bold">Name:</span> Saad Alam</span><br><br>
                          <span class="color-black"><span class="bold">Name:</span> Saad Alam</span><br><br>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-text-center uk-text-left@s">
                    <div class="uk-block-small">
                      <span class="color-black"><span class="bold">Origin:</span> American Born, Parents from Pakistan</span><br><br>
                      <span class="color-black"><span class="bold">Occupation:</span> CEO of Prime, Boaord Climb.Jobs, Advisor RallyPoint, Partner RachelMulherin.com</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="gridm uk-text-center">
              <div class="uk-block-large">
                <h2 class="color-black bold">Health Moment:</h2>
                <h3 class="color-black">When I turned 35 I started going through the aging process pretty quickly.</h3>
              </div>
            </div>
            <div class="gridxl uk-text-center">
              <img src="/prime/wp-content/uploads/2019/01/saadgraph.png">
            </div>
            <div class="uk-block steps bgcream">
              <div class="gridxs bg-white">
                <div class="uk-padding">
                  <div class="uk-grid uk-grid-small">
                    <div class="uk-width-1-6">
                      <div class="bg3 stepcircle">
                        <span class="color-white">1</span>
                      </div>
                    </div>
                    <div class="uk-width-5-6">
                      <p class="color-black">My thinking slowed down and my ability to think deeply through complex concepts was greatly reduced. I was not able to multi-task well.</p>
                    </div>
                    <div class="uk-width-1-6">
                      <div class="stepcircle">
                        <span class="color-white">2</span>
                      </div>
                    </div>
                    <div class="uk-width-5-6">
                      <p class="color-black">My thinking slowed down and my ability to think deeply through complex concepts was greatly reduced. I was not able to multi-task well.</p>
                    </div>
                    <div class="uk-width-1-6">
                      <div class="bg3 stepcircle">
                        <span class="color-white">3</span>
                      </div>
                    </div>
                    <div class="uk-width-5-6">
                      <p class="color-black">My thinking slowed down and my ability to think deeply through complex concepts was greatly reduced. I was not able to multi-task well.</p>
                    </div>
                    <div class="uk-width-1-6">
                      <div class="bg3 stepcircle">
                        <span class="color-white">4</span>
                      </div>
                    </div>
                    <div class="uk-width-5-6">
                      <p class="color-black">My thinking slowed down and my ability to think deeply through complex concepts was greatly reduced. I was not able to multi-task well.</p>
                    </div>
                    <div class="uk-width-1-6">
                      <div class="bg3 stepcircle">
                        <span class="color-white">5</span>
                      </div>
                    </div>
                    <div class="uk-width-5-6">
                      <p class="color-black">My thinking slowed down and my ability to think deeply through complex concepts was greatly reduced. I was not able to multi-task well.</p>
                    </div>
                    <div class="uk-width-1-6">
                      <div class="bg3 stepcircle">
                        <span class="color-white">6</span>
                      </div>
                    </div>
                    <div class="uk-width-5-6">
                      <p class="color-black">My thinking slowed down and my ability to think deeply through complex concepts was greatly reduced. I was not able to multi-task well.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="whatifelt uk-block bgcream">
              <div class="gridl">
                <div class="uk-padding">
                  <h2 class="color-black bold" style="text-transform: uppercase;">What I Felt</h2>
                </div>
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-1-2@m uk-text-center">
                    <img src="/prime/wp-content/uploads/2019/01/whatifelt.png">
                  </div>
                  <div class="uk-width-1-1 uk-width-1-2@m">
                    <p>My confidence started slipping since my ability to produce was declining, and my dreams about providing for my family were rattled.</p>
                    <p>I haven’t learned how to describe it, but the best word is frustration since I was doing all the things I THOUGHT were right, which worked great in the past, but they were producing the opposite results and I was getting worse.</p>
                    <p>I almost started resigning myself to the fact that I was just going through an aging process and it’s part of life. The feeling is like I was losing a battle against time that I had no chance of winning so I should just accept it.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="whatiwant uk-block bgcream">
              <div class="gridl">
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-1-2@m uk-text-center">
                    <img src="/prime/wp-content/uploads/2019/01/whatiwant.png" style="max-height: 550px;">
                  </div>
                  <div class="uk-width-1-1 uk-width-1-2@m">
                    <div class="uk-padding">
                      <p class="color-black">I want to feel like I am 31 again, which is when I felt like I was in my prime.</p>
                      <p class="color-black">I want to be able to work for 14 hours a day again without taking too many breaks so I can continue building great companies that impact the world.</p>
                      <p class="color-black">I want to be able to learn about new concepts and quickly implement them in my practice.</p>
                    </div>
                    <div class="uk-block">
                      <h2 class="color3 bold" style="text-transform: uppercase;">I will do<br> whatever it takes.</h2>
                    </div>
                  </div>
                </div>
                <div class="uk-block">
                  <div class="uk-grid">
                    <div class="uk-width-1-1 uk-width-2-3@m">
                      <div class="whatiwant">
                        <h3 class="color-black bold">What I want</h3>
                        <p class="color-black">Is abnormally extensive, and I know that it's longer than nearly anyone's, but I am willing to try and do whatever it takes to keep myself operating in the best condition from the inside out.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="bg-white uk-block current">
              <div class="gridxl">
                <div class="lefttext">
                  <h3 class="color-orange bold rotated">Current</h3>
                </div>
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                    <div class="bgorange uk-padding-small">
                      <h4 class="color-orange bold">Exercise</h4>
                      <ul>
                        <li class="color-black">Weights 3x per week</li>
                        <li class="color-black">Cardio 3x per week</li>
                        <li class="color-black">Spin</li>
                        <li class="color-black">Sprint</li>
                        <li class="color-black">HIT</li>
                      </ul>
                    </div>
                  </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgorange uk-padding-small">
                        <h4 class="color-orange bold">Supplements</h4>
                        <ul>
                          <li class="color-black">Weights 3x per week</li>
                          <li class="color-black">Cardio 3x per week</li>
                          <li class="color-black">Spin</li>
                          <li class="color-black">Sprint</li>
                          <li class="color-black">HIT</li>
                        </ul>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgorange uk-padding-small">
                        <h4 class="color-orange bold">Mindset</h4>
                        <ul>
                          <li class="color-black">Weights 3x per week</li>
                          <li class="color-black">Cardio 3x per week</li>
                          <li class="color-black">Spin</li>
                          <li class="color-black">Sprint</li>
                          <li class="color-black">HIT</li>
                        </ul>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgorange uk-padding-small">
                        <h4 class="color-orange bold">Products</h4>
                        <ul>
                          <li class="color-black">Weights 3x per week</li>
                          <li class="color-black">Cardio 3x per week</li>
                          <li class="color-black">Spin</li>
                          <li class="color-black">Sprint</li>
                          <li class="color-black">HIT</li>
                        </ul>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgorange uk-padding-small">
                        <h4 class="color-orange bold">Test</h4>
                        <ul>
                          <li class="color-black">Weights 3x per week</li>
                          <li class="color-black">Cardio 3x per week</li>
                          <li class="color-black">Spin</li>
                          <li class="color-black">Sprint</li>
                          <li class="color-black">HIT</li>
                        </ul>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                    <div class="bgorange uk-padding-small">
                      <h4 class="color-orange bold">Pharma</h4>
                      <ul>
                        <li class="color-black">Weights 3x per week</li>
                        <li class="color-black">Cardio 3x per week</li>
                        <li class="color-black">Spin</li>
                        <li class="color-black">Sprint</li>
                        <li class="color-black">HIT</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="bg-white adding">
              <div class="gridxl">
                <div class="lefttext">
                  <h3 class="color-teal bold rotated">Adding</h3>
                </div>
                <div class="uk-grid uk-grid-medium">
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                    <div class="bgteal uk-padding-small">
                      <h4 class="color-teal bold">Exercise</h4>
                      <ul>
                        <li class="color-black">Weights 3x per week</li>
                        <li class="color-black">Cardio 3x per week</li>
                        <li class="color-black">Spin</li>
                        <li class="color-black">Sprint</li>
                        <li class="color-black">HIT</li>
                      </ul>
                    </div>
                  </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgteal uk-padding-small">
                        <h4 class="color-teal bold">Supplements</h4>
                        <ul>
                          <li class="color-black">Weights 3x per week</li>
                          <li class="color-black">Cardio 3x per week</li>
                          <li class="color-black">Spin</li>
                          <li class="color-black">Sprint</li>
                          <li class="color-black">HIT</li>
                        </ul>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgteal uk-padding-small">
                        <h4 class="color-teal bold">Mindset</h4>
                        <ul>
                          <li class="color-black">Weights 3x per week</li>
                          <li class="color-black">Cardio 3x per week</li>
                          <li class="color-black">Spin</li>
                          <li class="color-black">Sprint</li>
                          <li class="color-black">HIT</li>
                        </ul>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgteal uk-padding-small">
                        <h4 class="color-teal bold">Products</h4>
                        <ul>
                          <li class="color-black">Weights 3x per week</li>
                          <li class="color-black">Cardio 3x per week</li>
                          <li class="color-black">Spin</li>
                          <li class="color-black">Sprint</li>
                          <li class="color-black">HIT</li>
                        </ul>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                      <div class="bgteal uk-padding-small">
                        <h4 class="color-teal bold">Test</h4>
                        <ul>
                          <li class="color-black">Weights 3x per week</li>
                          <li class="color-black">Cardio 3x per week</li>
                          <li class="color-black">Spin</li>
                          <li class="color-black">Sprint</li>
                          <li class="color-black">HIT</li>
                        </ul>
                      </div>
                    </div>
                  <div class="uk-width-1-1 uk-width-1-3@s uk-width-1-6@m ">
                    <div class="bgteal uk-padding-small">
                      <h4 class="color-teal bold">Pharma</h4>
                      <ul>
                        <li class="color-black">Weights 3x per week</li>
                        <li class="color-black">Cardio 3x per week</li>
                        <li class="color-black">Spin</li>
                        <li class="color-black">Sprint</li>
                        <li class="color-black">HIT</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="keypoints">
              <div class="uk-grid uk-grid-large uk-text-center uk-container-center">
                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-5@m uk-text-center">
                  <div class="bg3 circ">
                    <h4 class="color-white bold">Vitamin C</h4>
                  </div>
                </div>
                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-5@m uk-text-center">
                  <div class="bg3 circ">
                    <h4 class="color-white bold">Vitamin D3</h4>
                  </div>
                </div>
              
              <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-5@m uk-text-center">
                <div class="bg3 circ">
                  <h4 class="color-white bold">Krill Oil</h4>
                </div>
              </div>
              <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-5@m uk-text-center">
                <div class="bg3 circ">
                  <h4 class="color-white bold">Organifi Protein</h4>
                </div>
              </div>
              <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-5@m uk-text-center">
                <div class="bg3 circ">
                  <h4 class="color-white bold">Probiotic</h4>
                </div>
              </div>
            </div>
            <div class="testresults uk-block-large uk-background-cover" style="background-image: url('/prime/wp-content/uploads/2019/01/testresultsbg.png');">
              <div class="gridm">
                <div class="uk-block uk-text-center">
                  <h2 class="limec bold">Download all of my test results</h2>
                  <div class="spacer" style="height: 30px;">

                  </div>
                  <a href="#" class="btn squared lime">Download Now</a>
                </div>
              </div>
            </div>
          </li>
          <li>b</li>
          <li>c</li>
          <li>d</li>
          <li>e</li>
        </ul>
    </div>
  </section>


  {{-- <section class="stackcontent">
    <div class="switcherstuff" uk-slider data-uk-check-display>
      <div class="uk-position-relative">
        <div class="uk-slider-container">
          <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-3@s uk-child-width-1-4@m" uk-switcher="connect: .stacksul">
            <li>
              <a href="#">Saad Alam</a>
            </li>
            <li>
              <a href="#">Saad Alam2</a>
            </li>
            <li>
              <a href="#">Saad Alam3</a>
            </li>
            <li>
              <a href="#">Saad Alam</a>
            </li>
            <li>
              <a href="#">Saad Alam2</a>
            </li>
            <li>
              <a href="#">Saad Alam3</a>
            </li>
          </ul>
        </div>
        <a class="prev" href="#" uk-slider-item="previous">
          <span uk-icon="icon: chevron-left"></span>
        </a>
        <a class="next" href="#" uk-slider-item="next">
          <span uk-icon="icon: chevron-right"></span>
        </a>
      </div>
    </div>
    <div class="stacks uk-padding-large">
      <div class="gridxl">
        <ul id="stacksul" class="uk-switcher stacksul">
          <li>
            saad1
          </li>
          <li>
            saad2
          </li>
          <li>
            saad3
          </li>
          <li>
            saad1
          </li>
          <li>
            saad2
          </li>
          <li>
            saad3
          </li>
        </ul>
      </div>
    </div>
  </section> --}}
@endsection